var shoployal_base_url = "https://publicapp.shoployal.ie/loyal-customer";
var cus_attached_params = '';
function initFrame(url)
{
    (function($) {
        if(typeof __st.cid !== 'undefined' && getCookie("SL_unique_id") !== "")
        {
            function saveAppUser()
            {
                $.ajax({
                    url: shoployal_base_url + "/save-app-user"+cus_attached_params+"&unique_id=" + getCookie("SL_unique_id"),
                    success: function(resp){
                        document.cookie = "SL_unique_id" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                        if(resp != '')
                        {
              				var iframeDoc = $('#shoployal-frame')[0].contentDocument;
              				iframeDoc.write(resp);
                        }
                    },
                    error: function(){
                        setTimeout(saveAppUser, 5000);
                    }
                });
            }
            saveAppUser();
        }
        $.ajax({
            url: url,
            success: function(result){
              var iframeDoc = $('#shoployal-frame')[0].contentDocument;
              iframeDoc.write(result);
            }
        });
    })(jQuery);
}
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2)
    return parts.pop().split(";").shift();
  return "";
}
function setCookie(value){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + 1);
    var c_value=escape(value) + "; expires="+exdate.toUTCString()
                                + "; path=/";
    document.cookie="SL_unique_id" + "=" + c_value;
}
if(!document.getElementById("shoployal-frame"))
{
    var current_url = window.location.href;
    if(current_url.indexOf('uniqueId') > 0)
    {
        var pieces = current_url.split('uniqueId=');
        if(pieces[1] && pieces[1] != '')
        {
            setCookie(pieces[1]);
        }
    }
    var shopURL = window.location.host;
    var iframe = document.createElement('iframe');
    var url = '';
    iframe.id = "shoployal-frame";
    iframe.style.position = "fixed";
    iframe.style.right = "0px";
    iframe.style.bottom = "0px";
    iframe.style.zIndex = "10";
    iframe.style.height = "100px";
    iframe.style.width = "240px";
    iframe.style.border = "none";
    if(typeof __st.cid != 'undefined')
    {
        url = shoployal_base_url + '/dashboard?shop='+shopURL + '&customer=' + __st.cid + '&required=layout';
        cus_attached_params = '?shop='+shopURL + '&customer=' + __st.cid;
    }
    else
    {
        url = shoployal_base_url + '?shop='+shopURL;
        cus_attached_params = '?shop='+shopURL;
    }
    document.body.appendChild(iframe);

    if (typeof jQuery === 'undefined') {
        var jq = document.createElement("script");
        jq.type = "text/javascript";
        jq.src = "https://code.jquery.com/jquery-2.2.4.min.js";
        jq.onload = function(){
            initFrame(url);
        };
        document.body.appendChild(jq);
    } else {
        initFrame(url);
    }
}
