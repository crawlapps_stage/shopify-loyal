function Toastr(data) {
    $('div.modal button.close').click();
    $(document).find('.modal-backdrop').remove();
    if (data[0] == 'success')
        toastr.success(data[1]);
    else if (data[0] == 'error')
        toastr.error(data[1]);
    else if (data[0] == 'info')
        toastr.info(data[1]);
    else if (data[0] == 'warning')
        toastr.warning(data[1]);
    handleAJAX(window.location.href)
}

function handleAJAX(url, pushState = !0, action = 'call') {
    $('#loader').fadeIn();
    $.ajax({
        type: "GET",
        cache: !1,
        async: true,
        headers: {
          "cache-control": "no-cache"
        },
        url: url, 
        success: function(data) {
            $('#loader').fadeOut();
            if (action == 'call') {
                if (Array.isArray(data)) {
                    Toastr(data)
                } else {
                    $('.sidebar-nav li').each(function() {
                        if ($(this).find('a').attr('href') == url) {
                            $('.sidebar-nav li').removeClass('active');
                            $(this).addClass('active')
                        }
                    })
                }
                $('div#page-content').html(data);
                initPlugins();
            } else if (action == 'action') {
                $('#del-confirm-popup button.close').click();
                $('#approve-confirm-popup button.close').click();
                $('.modal-backdrop.fade.show').remove();
                Toastr(data);
            } else if (action == 'detail') {
                if (Array.isArray(data)) {
                    Toastr(data)
                } else {
                    $('#append-modal').html(data);
                    $(document).find('#modal-toggle').click()
                }
            }
            if (pushState)
                window.history.pushState(url, '', url)
        },
        error: function(){
            toastr.error('Unknown Error!');
            $('#loader').fadeOut();
        }
    });
}
$(document).on('click', '.call-url,.action-url,.detail-url', function(event) {
    event.preventDefault();
    var url = $(this).attr('href');
    var action = '';
    if ($(this).hasClass('call-url')) {
        handleAJAX(url);
    } else if ($(this).hasClass('action-url'))
        handleAJAX(url, !1, 'action');
    else {
        handleAJAX(url, !1, 'detail')
    }
    if($(this).closest('.nav-li').length)
    {
        $('.nav-li').removeClass('active');
        $(this).closest('.nav-li').addClass('active');
        $('.bars').click();
    }
});
$(document).on('click', '.del-btn', function(event) {
    event.preventDefault();
    $('#del-btn-global').attr('href', $(this).attr('href'));
    $('#del-popup').modal('show');
});
$(document).on('click', '.approve-btn', function(event) {
    event.preventDefault();
    $('#approve-btn-global').attr('href', $(this).attr('href'));
    $('#approve-confirm-popup-toggler').click()
});
$('.logout-btn').click(function(event) {
    event.preventDefault();
    $('#logout-form').submit()
});
$(document).on('submit', 'form', function(event) {
    if (!$(this).hasClass('manual')) {
        event.preventDefault();
        $('#loader').fadeIn();
        var form = $(this);
        var data = new FormData(this);
        $.ajax({
            type: 'POST',
            data: data,
            cache: !1,
            contentType: !1,
            processData: !1,
            url: $(form).attr('action'),
            async: true,
            headers: {
              "cache-control": "no-cache"
            },
            success: function(response) {
                if (response[0] == 'success') {
                    if($(form).closest('.modal').length)
                    {
                        $('.modal-backdrop').remove();
                        $('body').removeClass('modal-open');
                    }
                    
                    toastr.success(response[1]);
                    if (response['force_redirect'])
                        window.location.href = response['force_redirect'];
                    else if(response[2])
                        handleAJAX(response[2]);
                    else
                        handleAJAX(window.location.href);
                        
                } else if (response[0] == 'error') {
                    toastr.error(response[1])
                }
                else if(response[0] == 'info'){
                    toastr.info(response[1]);
                }
                else
                    $('#loader').fadeOut();
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {
                    var errorObj = xhr.responseJSON.errors;
                    $.map(errorObj, function(value, index) {
                        $(form).find('[name="' + index + '"]').closest('div.form-group').find('div.alert').remove();
                        var appendIn = $(form).find('[name="' + index + '"]').closest('div.form-group');
                        if (!appendIn.length) {
                            toastr.error(value[0])
                        } else {
                            if (!$(appendIn).find('div.alert').length) {
                                if ($(appendIn).hasClass('dropify-wrapper'))
                                    appendIn = $(appendIn).closest('div');
                                $(appendIn).append('<div class="alert alert-danger" style="padding: 1px 5px;font-size: 12px"> ' + value[0] + '</div>')
                            }
                        }
                    });
                } else {
                    toastr.error('Unknown Error!')
                }
                $('#loader').fadeOut()
            }
        })
    }
});
window.onpopstate = function(e) {
    setTimeout(function() {
        handleAJAX(window.location.href, !1)
    }, 50)
}