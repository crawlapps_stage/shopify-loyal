<!DOCTYPE html>
<html>
<head>
	<title>Choose Subscription Plan | ShopLoyal</title>
	<meta charset="utf-8">
	<link href="{{ asset('shop/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Quicksand:300,400,700);
		* {
		  margin: 0;
		  padding: 0;
		  box-sizing: border-box;
		  font-family: "Quicksand";
		  font-weight: normal;
		  color: #252435;
		}

body {
	background-color: #fff;
}
		.container {
		  width: 100%;
		  display: flex;
		  justify-content: center;
		  flex-wrap: wrap;
		}

		.col {
		  width: 350px;
		  min-height: 565px;
		  text-align: center;
		  height: 100%;
		  margin: 20px;
		  height: 565px;
		}

		.card {
		  height: 100%;
		  padding: 1rem;
		  margin-bottom: 1rem;
		  display: flex;
		  flex-direction: column;
		  justify-content: space-between;
		  box-shadow: 1px 1px 25px 0px rgba(143, 85, 85, 0.1);
		}

		.card-header {
		  height: 30%;
		  display: flex;
		  flex-direction: column;
		  justify-content: space-around;
		  margin-bottom: 2rem;
		}
		.card-header .title {
		  font-size: 1.5em;
		  font-weight: bold;
		  color: rgba(37, 36, 53, 0.9);
		  letter-spacing: 2px;
		}
		.card-header .title span {
		  color: #ff5a5f;
		}
		.card-header .price {
		  font-size: 2.5em;
		  color: #ff5a5f;
		  width: 30%;
		  margin: 0 auto;
		  padding: 10px;
		}
		.card-header .price span {
		  font-size: 0.5em;
		  color: inherit;
		}
		.card-header .description {
		  color: rgba(37, 36, 53, 0.7);
		}

		.features {
		  height: 70%;
		  text-align: left;
		  list-style: none;
		  padding: 1rem;
		}
		.features li {
		  color: rgba(37, 36, 53, 0.7);
		  margin-bottom: 1rem;
		}

		.line {
		  width: 25%;
		  margin: 15px auto 0;
		  height: 3px;
		  background-color: rgba(255, 90, 95, 0.9);
		}

		.fa-check{
			color: #5ECD1A;
		}
		.fa-times{
			color: #FF6A6F;
		}
		.text-center{
			text-align: center;
		}
		.mb-3{
			margin-bottom: 3%;
		}
		.bold{
			font-weight: bold;
		}
		input,.StripeElement {
		  box-sizing: border-box;

		  height: 40px;

		  padding: 10px 12px;

		  border: 1px solid #ccc;
		  border-radius: 4px;
		  background-color: white;
		  transition: box-shadow 150ms ease;
		}

		.StripeElement--focus {
		  box-shadow: 0 1px 3px 0 #cfd7df;
		}

		.StripeElement--invalid {
		  border-color: #fa755a;
		}

		.StripeElement--webkit-autofill {
		  background-color: #fefde5 !important;
		}
		.alert{
		    padding: 6px 12px;
		    margin-bottom: 0px;
		    font-weight: bold;
		}
	</style>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('shop/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    @php
        $plans = shopifyPlans();
    @endphp
	<h1 class="text-center mb-3 bold">CHOOSE SUBSCRIPTION PLAN</h1>
	<div class="container">
	  <div class="col">
	    <div class="card">
	      <div class="card-header">
	        <h2 class="title">Loyalty Starter</h2>
	        <p class="price">
	          {{ $plans['loyalty_starter']['price'] }}
	          <span>€</span>
	        </p>
	        <span class="description">Free Plan</span>
	        <div class="line"></div>
	      </div>

	      <ul class="features">
	        <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
	        <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
	        <li><i class="fa fa-check"></i> 1000 customers</li>
	      </ul>
	      @if(@$shop->plan != $plans['loyalty_starter']['id'] )
	      <a href="{{ route('charge.payment',['plan_id' => $plans['loyalty_starter']['id']]) }}" target="_blank" class="btn btn-success">Select</a>
	      @else
	      <div class="alert alert-success">
	      	Current Plan
	      </div>
	      @endif
	    </div>
	  </div>
	  <div class="col">
	    <div class="card">
	      <div class="card-header">
	        <h2 class="title">Online Loyalty</h2>
	        <p class="price">
                {{ $plans['online_loyalty']['price'] }}
	          <span>€</span>
	        </p>
	        <span class="description">Perfect for Online Store</span>
	        <div class="line"></div>
	      </div>

	      <ul class="features">
	        <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
	        <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
	        <li><i class="fa fa-check"></i> 10,000 customers</li>
	        <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
	        <li><i class="fa fa-check"></i> Email support</li>
	      </ul>
	      @if(@$shop->plan != $plans['online_loyalty']['id'])
	      <a href="{{ route('charge.payment',['plan_id' => $plans['online_loyalty']['id']]) }}" target="_blank" class="btn btn-success" >Start Free Trial</a>
	      @else
	      <div class="alert alert-success">
	      	Current Plan
	      </div>
	      @endif
	    </div>
	  </div>
	  <div class="col">
	    <div class="card">
	      <div class="card-header">
	        <h2 class="title">Online and Offline</h2>
	        <p class="price">
                {{ $plans['online_offline']['price'] }}
	          <span>€</span>
	        </p>
	        <span class="description">Perfect for physical stores with online presence</span>
	        <div class="line"></div>
	      </div>

	      <ul class="features">
	        <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
	        <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
	        <li><i class="fa fa-check"></i> Unlimited customers</li>
	        <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
	        <li><i class="fa fa-check"></i> iOS and Anroid integration (custom mobile app available)</li>
	        <li><i class="fa fa-check"></i> Monthly reporting</li>
	        <li><i class="fa fa-check"></i> POS integration</li>
	        <li><i class="fa fa-check"></i> 24/7 Customer service</li>
	      </ul>
	      @if(@$shop->plan != $plans['online_offline']['id'])
	      <a href="{{ route('charge.payment',['plan_id' => $plans['online_offline']['id']]) }}" target="_blank" class="btn btn-success" >Start Free Trial</a>
	      @else
	      <div class="alert alert-success">
	      	Current Plan<br>
	      </div>
	      @endif
	    </div>
	  </div>
	  <div class="col">
	    <div class="card">
	      <div class="card-header">
	        <h2 class="title">Enterprise</h2>
	        <p class="price">
                {{ $plans['enterprise']['price'] }}
	          <span>€</span>
	        </p>
	        <span class="description">For more 5+ stores and heavy online presence</span>
	        <div class="line"></div>
	      </div>

	      <ul class="features">
	        <li><i class="fa fa-check"></i> Easy to install app on Shopify</li>
	        <li><i class="fa fa-check"></i> Easy to use Dashboard for full control</li>
	        <li><i class="fa fa-check"></i> Unlimited customers</li>
	        <li><i class="fa fa-check"></i> White Label - Use your colours and your logo</li>
	        <li><i class="fa fa-check"></i> iOS and Anroid integration (custom mobile app available)</li>
	        <li><i class="fa fa-check"></i> Live reporting</li>
	        <li><i class="fa fa-check"></i> Custom POS integration</li>
	        <li><i class="fa fa-check"></i> 24/7 Customer service</li>
	        <li><i class="fa fa-check"></i> Personal Assistant and Web Training</li>
	      </ul>

	      @if(@$shop->plan != $plans['enterprise']['id'])
	      <a href="{{ route('charge.payment',['plan_id' => $plans['enterprise']['id']]) }}" target="_blank" class="btn btn-success">Start Free Trial</a>
	      @else
	      <div class="alert alert-success">
	      	Current Plan<br>
	      </div>
	      @endif
	    </div>
	  </div>
	</div>
</div>

<!-- Jquery Core Js -->
<script src="{{ asset('shop/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('shop/plugins/bootstrap-toastr/toastr.min.js') }}"></script>

<!-- Bootstrap Core Js -->
<script src="{{ asset('shop/plugins/bootstrap/js/bootstrap.js') }}"></script>

</body>
</html>
