<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.dashboard') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('Loyalty Levels') }}</span>
		</h6>
	</div>
	<div class="col-12">
	    @php error_reporting(0) @endphp
		@foreach($levels as $lvl)
		<div class="col-12 list-item-custom d-flex mb-2 {{ authUser()->level_id == $lvl->id ? 'active-clevel' : '' }} clevel">
			<div class="desc">
				<h6>
					<span><b>{{ $lvl->title }}</b></span>
					@if($loop->iteration != 1)
					<br>
					<small>
					    Összesen {{ $lvl->required_points }} Ft vásárlás szükséges
                    </small>
                    @else
                    <br><br>
                    @endif
				</h6>
			</div>
		</div>
		@endforeach
	</div>
</div>