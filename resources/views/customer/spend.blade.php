<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.dashboard') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('Spend Points') }}</span>
		</h6>
		@if(authUser()->level)
		<h6 class="red-text text-center"><b>Your Level is</b> <b><b>{{ authUser()->level->title }}</b></b></h6>
		@endif
	</div>
	<div class="col-12">
		<div class="tab-content pt-3">
		  	<div class="tab-pane fade in show active" id="panel11" role="tabpanel">
		  		<div class="">
		  			@foreach($discounts as $d)
		  			@if($d->points <= authUser()->remaining_points)
		  			@php $activeDiscount = true; @endphp
		  			@else
		  			@php $activeDiscount = false; @endphp
		  			@endif
		  			<div class="col-12 list-item-custom d-flex mb-2 {{ $activeDiscount ? 'active-discount' : '' }}">
		  			    @if($activeDiscount)
		  				<a target="_top" href="https://{{ userShop()->url }}/discount/{{ $d->code }}">
		  				@endif
		  				<div class="icon">
		  					<i class="fas fa-tags"></i>
		  				</div>
		  				<div class="desc">
		  					<h6>
		  						<span>
		  							<b>
				  						@if($d->discount_type != 3)
					  						@if($d->discount_type == 1)
					  						@php $discount = $d->discount . '%'; @endphp
					  						@else
					  						@php $discount = userShop()->currency . $d->discount @endphp
					  						@endif
					  						{{ $discount }} off
				  						@else
				  						Free Shipping
				  						@endif
				  					</b>
				  				</span> <br>
		  						<small>{{ $d->points }} points</small>
		  					</h6>
		  				</div>
		  				@if($activeDiscount)
		  				</a>
		  				@endif
		  			</div>
		  			@endforeach
		  		</div>
		  	</div>
		</div>
	</div>
</div>