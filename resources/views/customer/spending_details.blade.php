<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.spendings') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('My Spendings') }} <small>({{ __('Details') }})</small></span>
		</h6>
	</div>
	<div class="col-12 mt-2 table-responsive">
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th scope="col" width="80%"><b>{{ __('Product') }}</b></th>
		      <th scope="col" width="20%"><b>{{ __('Price') }}</b></th>
		      <th scope="col" width="20%"><b>{{ __('Status') }}</b></th>
		    </tr>
		  </thead>
		  <tbody>
		    @foreach($details as $d)
		    <tr>
		      	<td width="80%">{{ $d->title }}</td>
		      	<td width="20%">{{ $d->amount }}</td>
		      	<td width="20%">
		      	    @if($d->status == 0)
		      	    <span class="label label-info">Pending</span>
		      	    @elseif($d->status == 1)
		      	    <span class="label label-info">Delivered</span>
		      	    @elseif($d->status == 2)
		      	    <span class="label label-info">Fulfilled</span>
		      	    @else
		      	    <span class="label label-info">Refunded</span>
		      	    @endif
		        </td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	</div>
</div>