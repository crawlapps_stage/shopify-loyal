<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.dashboard') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('Add Barcode') }}</span>
		</h6>
	</div>
	<div class="col-12">
	    <form action="" id="barcodes">
	        @if(authUser()->plastic_card_barcode == null)
	        <label class="blabel">
	            {{ __('Enter 13 digit barcode') }} 
	        </label>
	        <input type="text" min="13" class="form-control mb-2 binput" placeholder="{{ __('Add Barcode') }}" name="barcode">
	        @if(authUser()->plastic_card_barcode1 == null)
	        <label class="blabel red-text mb-2">{{ __('You can add one more barcode') }}</label>
	        @endif
	        @else
	        <label class="blabel">{{ __('Your Barcode') }}: </label>
    	    <input type="text" readonly class="form-control mb-1 binput" value="{{ authUser()->plastic_card_barcode }}">
    	    @endif
    	    @if(!empty($error))
    	    <div class="alert alert-danger">{{ __($error) }}</div>
    	    @elseif(!empty($success))
    	    <div class="alert alert-success">{{ __($success) }}</div>
    	    @endif
    	    @if(authUser()->plastic_card_barcode)
    	    <hr />
	        @if(authUser()->plastic_card_barcode1 == null)
	        <label class="blabel">{{ __('Enter 13 digit additional barcode') }}</label>
	        <input type="text" min="13" class="form-control mb-1 binput" placeholder="{{ __('Add Additional Barcode') }}" name="barcode1">
	        @else
	        <label class="blabel">{{ __('Your Additional Barcode') }}: </label>
    	    <input type="text" readonly class="form-control mb-1 binput" value="{{ authUser()->plastic_card_barcode1 }}">
    	    @endif
    	    @if(!empty($error1))
    	    <div class="alert alert-danger">{{ __($error1) }}</div>
    	    @elseif(!empty($success1))
    	    <div class="alert alert-success">{{ __($success1) }}</div>
    	    @endif
	        @endif
	        @if(authUser()->plastic_card_barcode1 == null || authUser()->plastic_card_barcode == null)
	        <button class="btn btn-sm theme-bg btn-primary btn-block binput">{{ __('Link Barcode') }}</button>
	        @endif
	    </form>
	</div>
</div>
<script>
    $('form#barcodes').submit(function(e){
        e.preventDefault();
        handleAJAX(
            "{{ route('link.barcode') }}", 
            '&barcode=' + $('input[name="barcode"]').val()
             + '&barcode1=' + $('input[name="barcode1"]').val()
        );
    });
</script>