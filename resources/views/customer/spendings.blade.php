<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.dashboard') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('My Spendings') }}</span>
		</h6>
	</div>
	@if(sizeof($completed) == 0 && sizeof($pendings) == 0)
	<h6 class="text-center mt-4 mb-4 red-text col-md-12" style="font-weight: bold">{{ __('No transaction found') }}!</h6>
	@endif
	@if(sizeof($completed))
	<div class="col-12 mt-2 table-responsive">
	    <h6 class="red-text">
  			<i class="fa fa-caret-right"></i> 
  			<b>{{ __('Prev Purchases') }}</b>
  		</h6>
	    @foreach($completed as $s)
	    <table class="table table-bordered">
	        <tbody>
    		    <tr>
    		      	<th width="50%">{{ __('Spent') }}</th>
    		      	<td>{{ $s->spent_amount }}</td>
                </tr>        		      	
                <tr>
    		      	<th width="50%">{{ __('Discount') }}</th>
    		      	<td>
    		      		{{ $s->discount == 'NaN' ? '0%' : $s->discount }}
    		      	</td>
    		    </tr>
    		    <tr>
    		      	<th width="50%">{{ __('Date of transaction') }}</th>
    		      	<td>
    		      		{{ $s->dot }}
    		      	</td>
    		    </tr>
    		    <tr>
    		        <th width="50%">{{ __('Order') }}</th>
    		        <td>
    		            @if($s->status_url)
    		      		<a href="{{ $s->status_url }}" class="btn btn-primary white-text btn-sm" target="_top">
    		      			{{ __('View Order') }} <i class="fas fa paper-plane"></i>
    		      		</a>
    		      		@else
    		      		<a href="{{ route('spending.details', $s->id) }}" class="btn btn-primary white-text btn-sm action">
    		      			{{ __('View Order') }} <i class="fas fa paper-plane"></i>
    		      		</a>
    		      		@endif
    		      	</td>
    		    </tr>
    	    </tbody>
	    </table>
	    @endforeach
	</div>
	@endif
	@if(sizeof($pendings))
	<div class="col-12 mt-2 table-responsive">
	    <h6 class="red-text">
  			<i class="fa fa-caret-right"></i> 
  			<b>{{ __('Pending Purchases') }}</b>
  		</h6>
	    @foreach($pendings as $s)
	    <table class="table table-bordered">
	        <tbody>
    		    <tr>
    		      	<th width="50%">{{ __('Spent') }}</th>
    		      	<td>{{ $s->spent_amount }}</td>
                </tr>        		      	
                <tr>
    		      	<th width="50%">{{ __('Discount') }}</th>
    		      	<td>
    		      		{{ $s->discount == 'NaN' ? '0%' : $s->discount }}
    		      	</td>
    		    </tr>
    		    <tr>
    		      	<th width="50%">{{ __('Date of transaction') }}</th>
    		      	<td>
    		      		{{ $s->dot }}
    		      	</td>
    		    </tr>
    		    <tr>
    		        <th width="50%">{{ __('Order') }}</th>
    		        <td>
    		            @if($s->status_url)
    		      		<a href="{{ $s->status_url }}" class="btn btn-primary white-text btn-sm" target="_top">
    		      			{{ __('View Order') }} <i class="fas fa paper-plane"></i>
    		      		</a>
    		      		@else
    		      		<a href="{{ route('spending.details', $s->id) }}" class="btn btn-primary white-text btn-sm action">
    		      			{{ __('View Order') }} <i class="fas fa paper-plane"></i>
    		      		</a>
    		      		@endif
    		      	</td>
    		    </tr>
    	    </tbody>
	    </table>
	    @endforeach
	</div>
	@endif
</div>