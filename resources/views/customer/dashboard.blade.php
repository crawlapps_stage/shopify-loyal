<div class="row">
	<div class="col-12">
		<h2 class="text-center">
		    @php $name = explode(" ", $customer->name); @endphp
			{{ __('Hi') }} {{ $name[0] }}!
		</h2>
		<h6 class="inner-heading text-center">
		    {{ __('You Have') }}: <b class="red-text">{{ $customer->remaining_points }}</b> Ft
		</h6>
	</div>
	<div class="col-12">
		<div class="mb-2 mt-2">
			<a href="{{ route('customer.earn') }}" class="btn btn-outline-danger waves-effect btn-block action">@lang('Earn Points')</a>
		</div>
		<div class="mb-2">
			<a href="{{ route('customer.spend') }}" class="btn btn-outline-info waves-effect btn-block action">@lang('Spend Points')</a>
		</div>
		<div class="mb-2">
			<a href="{{ route('customer.earnings') }}" class="btn btn-outline-success waves-effect btn-block action">@lang('My Rewards')</a>
		</div>
		<div class="mb-2">
			<a href="{{ route('customer.spendings') }}" class="btn btn-outline-primary waves-effect btn-block action">@lang('Point Spendings')</a>
		</div>
		<!--<div class="mb-2">-->
		<!--	<a href="{{ route('customer.loyalty.levels') }}" class="btn btn-outline-primary waves-effect btn-block action">@lang('Loyalty Levels')</a>-->
		<!--</div>-->
		@if(userShop()->plan == 3)
		<div class="mb-2">
			<a href="{{ route('add.barcode') }}" class="btn theme-border theme-color waves-effect btn-block action p-84rem">{{ __('Add Barcode') }}</a>
		</div>
		@endif
	</div>
</div>