<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.index') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('Points') }}</span>
		</h6>
	</div>
	<div class="col-12">
		<h5>
			Earn Points for different actions & convert Points into Rewards.
		</h5>
		<ul class="nav md-pills nav-justified custom-pills">
		  <li class="nav-item">
		    <a class="nav-link active" data-toggle="tab" href="#panel11" role="tab">Discounts</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" data-toggle="tab" href="#panel12" role="tab">Offers</a>
		  </li>
		</ul>
		<div class="tab-content pt-3">
		  	<div class="tab-pane fade in show active" id="panel11" role="tabpanel">
		  		<div class="">
		  			@foreach($events as $e)
		  			<div class="mt-2 mb-2">
		  				<h6 class="red-text">
		  					<i class="fa fa-caret-right"></i> 
		  					<b>{{ $e->name }}</b>
		  				</h6>
		  			</div>
		  			@foreach($e->discounts as $d)
		  			<div class="col-12 list-item-custom d-flex mb-2">
		  				<div class="icon">
		  					<i class="fas fa-tags"></i>
		  				</div>
		  				<div class="desc">
		  					<h6>
		  						<span>
		  							<b>
				  						@if($d->discount_type != 3)
					  						@if($d->discount_type == 1)
					  						@php $discount = $d->discount . '%'; @endphp
					  						@else
					  						@php $discount = $shop->currency . $d->discount @endphp
					  						@endif
					  						{{ $discount }} off discount
				  						@else
				  						Free Shipping
				  						@endif
				  					</b>
				  				</span> <br>
		  						<small>{{ $d->points }} points</small>
		  					</h6>
		  				</div>
		  			</div>
		  			@endforeach
		  			@endforeach
		  		</div>
		  		<hr>
		  		<div class="">
		  			<div class="mt-2 mb-2">
		  				<h6 class="red-text">
		  					<i class="fa fa-caret-right"></i> 
		  					<b>General Discounts</b>
		  				</h6>
		  			</div>
		  			@foreach($discounts as $d)
		  			<div class="col-12 list-item-custom d-flex mb-2">
		  				<div class="icon">
		  					<i class="fas fa-tags"></i>
		  				</div>
		  				<div class="desc">
		  					<h6>
		  						<span>
		  							<b>
				  						@if($d->discount_type != 3)
					  						@if($d->discount_type == 1)
					  						@php $discount = $d->discount . '%'; @endphp
					  						@else
					  						@php $discount = $shop->currency . $d->discount @endphp
					  						@endif
					  						{{ $discount }} off
				  						@else
				  						Free Shipping
				  						@endif
				  					</b>
				  				</span> <br>
		  						<small>{{ $d->points }} points</small>
		  					</h6>
		  				</div>
		  			</div>
		  			@endforeach
		  		</div>
		  	</div>
		  	<div class="tab-pane fade mt-1" id="panel12" role="tabpanel">
			    <div class="">
			    	@foreach($campaigns as $c)
			    	<div class="mt-2 mb-2">
		  				<h6 class="red-text">
		  					<i class="fa fa-caret-right"></i> 
		  					<b>{{ $c->name }}</b>
		  				</h6>
		  			</div>
		  			@foreach($c->campaignItem as $ci)
			    	<div class="col-12 list-item-custom d-flex mb-2">
		  				<div class="image">
		  					<img class="card-img-top" src="{{ $ci->image }}" alt="{{ $ci->title }}">
		  				</div>
		  				<div class="desc">
		  					<h6>
		  						<span><b>{{ $ci->title }}</b></span><br>
		  						<small>{{ $c->discount->points }}</small>
		  						@if($c->discount->discount_type == 1)
		  						@php $discount = $c->discount->discount . '%'; @endphp
		  						@else
		  						@php $discount = $shop->currency . $c->discount->discount @endphp
		  						@endif
		  						<span class="badge badge-info pull-right">{{ $discount }} off</span>
		  					</h6>
		  				</div>
		  			</div>
		  			@endforeach
		  			@endforeach
			    </div>
		  	</div>
		</div>
	</div>
</div>