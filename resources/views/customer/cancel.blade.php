<div class="row">
    @if(empty($cancelled))
    <div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.earn') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ __('Leave Club') }}</span>
		</h6>
	</div>
    <div class="col-12">
        <p class="mb-3">
            @lang('By terminating my membership, I acknowledge that my account will be deleted. My forint balance so far and my eligibility for Loyalty Club Discontinuation.')
        </p>
        <p class="mb-0">
            <b>
                @lang('Are you sure you want to terminate your Loyalty Club membership?')
            </b>
        </p>
        <a href="{{ route('customer.cancel') }}" class="btn btn-danger m-0 action"><b>@lang('Yes')</b></a>
        <a href="{{ route('customer.dashboard') }}" class="btn btn-primary action"><b>@lang('Nem')</b></a>
    </div>
    @else
    <div class="col-12 text-center">
        <h4 class="mt-5 mb-5 red-text"><b>@lang('We have terminated your membership')</b></h4>
    </div>
    @endif
</div>