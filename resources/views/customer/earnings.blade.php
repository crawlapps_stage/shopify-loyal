<div class="row">
	<div class="col-12">
		<h6 class="text-center inner-heading">
			<a href="{{ route('customer.dashboard') }}" class="float-left action">
				<i class="fas fa-chevron-left"></i>
			</a>
			<span>{{ TR('Earnings') }}</span>
		</h6>
	</div>
	<div class="col-12 mt-2 table-responsive">
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th scope="col">Event</th>
		      <th scope="col">Reward</th>
		    </tr>
		  </thead>
		  <tbody>
		    @foreach($earnings as $e)
		    <tr>
		      	<td>
		      		{{ $e->occasion }}
		      		@if($e->order_id)
		      		<a href="{{ $e->status_url }}" class="btn btn-info btn-sm" target="_top">
		      			View Order <i class="fas fa paper-plane"></i>
		      		</a>
		      		@endif
		      	</td>
		      	<td>{{ $e->total_points }} Points</td>
		    </tr>
		    @endforeach
		  </tbody>
		</table>
	</div>
</div>