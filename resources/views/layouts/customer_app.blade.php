<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Material Design Bootstrap</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="{{ asset('customer/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="{{ asset('customer/css/mdb.min.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet"> 
  <style type="text/css">
    body{
        font-family: "Quicksand";
    }
    .modal-body{
      max-height: 450px;
      overflow-y: scroll;
      overflow-x: hidden;
      padding: 10px !important;
    }
    .modal-footer{
      display: block;
    }
    .list-item-custom{
      background-color: #f9f9f9;
      border-radius: 4px;
      padding: 0px;
    }
    .list-item-custom .icon{
      font-size: 23px;
      padding-top: 4px;
      padding-left: 15px
    }
    .list-item-custom .image{
      width: 70px;
    }
    .list-item-custom .desc{
      padding-left: 15px;
      padding-right: 15px;
      padding-top: 5px;
    }
    .list-item-custom .desc h6{
      font-size: 14px;
    }
    .list-item-custom .desc h6 span{
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
    }
    .list-item-custom .desc h6 b{
      font-weight: bold;
    }
    .list-item-custom .desc small{
      color: gray;
    }
    .list-item-custom .badge{
      position: absolute;
      right: 15px;
      margin-top: 2px;
      box-shadow: none;
    }
    .custom-pills a{
      border-bottom: 2px solid transparent;
      color: #ff3547;
    }
    .custom-pills a.active{
      border-bottom: 2px solid #ff3547;
      font-weight: bold;
    }
    .inner-heading{
      font-weight: bold;
      font-size: 14px;
      letter-spacing: 2px;
      margin-bottom: 10px;
    }
    .inner-heading .fas{
      color: #616161;
    }
   #loader{
    display: flex;
    justify-content: center;
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    padding-top: inherit;
    background: rgba(255,255,255,.8);
   } 
   .reward-toggler{
    position: fixed;
    right: 15px;
    bottom: 15px;
    z-index: 10;
   }
   .active-discount a{
       display: inherit;
   }
   .active-clevel{
       background: #e2e2e2;
   }
   .active-clevel .desc{
       border-left: 4px solid #004d9a !important;
   }
   .active-clevel .desc, .active-clevel .desc small{
      color: #004d9a;
   }
   .list-item-custom.clevel .desc{
       border-left: 4px solid #ccc;
   }
   table.table th{
       font-weight: 500;
   }
   table.table td, table.table th{
       padding: 4px;
       font-size: 12px;
   }
   .table-bordered td, .table-bordered th {
        border: 1px solid #ddd;
    }
   table.table td .btn{
       padding: 1px 5px !important;
   }
   .table-responsive h6{
       font-size: 14px;
   }
   @php $userShop = App\Shop::whereUrl($_GET['shop'])->first(); @endphp
   .theme-bg{
       background: <?php echo $userShop->background_color; ?> !important;
   }
   .theme-color{
       color: <?php echo $userShop->background_color; ?> !important;
   }
   .theme-border{
       border:1px solid <?php echo $userShop->background_color; ?> !important;
   }
   .fw500{
       font-weight: 500;
   }
   .p-84rem{
       padding: .84rem;
   }
   .blabel{
        font-weight: bold;margin-bottom: 2px;font-size: 12px;
    }
    .binput{
        font-size: 13px;
    }
    .alert-danger,.alert-success{
        padding: 5px 10px;
        font-size: 12px;
        font-weight: 500;
        margin-bottom: 10px;
    }
  </style>
</head>

<body style="background: transparent">
  <div id="app">
    <button type="button" class="btn btn-danger theme-bg reward-toggler fw500" data-toggle="modal" data-target="#modalDiscount">{{ $userShop->button_text }}</button>

    <div class="modal fade right" id="modalDiscount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" style="height: auto;bottom: 0px;top: auto">
      <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-danger modal-sm" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header theme-bg">
            <p class="heading fw500">
              {{ $userShop->header_text }}
            </p>
            <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="white-text">&times;</span>
            </button>
          </div>
          <!--Body-->
          <div class="modal-body">
            <div class="alert alert-danger" id="ajax-error" role="alert" style="display: none">
              {{ __('Something went wrong, please try again') }}.
            </div>
            @if(!empty($joined))
            <div class="alert alert-success" id="joining">
              {{ __('Thank you for joining') }}.
            </div>
            @endif
            <div id="page-content">
              @if(!empty($forJoining))
              <div class="alert alert-danger" id="joining-error" role="alert" style="display: none">
                  {{ __('Accept terms & conditions to continue') }}.
              </div>
              <div class="mt-1 mb-1 text-center">
                <h5 class="mb-3"><b>{{ __('Join Reward Program & Start Earning Now') }}</b></h5>
                <form action="{{ route('customer.join') }}" class="text-left" id="joining-form">
                    @if($userShop->plan == 3)
        	        <label class="blabel mb-2" style="font-size: 13px;font-weight: normal">
        	            {{ __('Loyalty card code is optional and not required. You only need to get a Loyalty Card in the store.If you cannot find a problem now, you can add it to your account later in up to two cases') }}
        	        </label>
        	        <input type="text" min="13" class="form-control mb-1" placeholder="{{ __('Add Barcode') }}" name="barcode" style="font-size: 13px">
        	        <div class="custom-control custom-checkbox mb-2 mt-2">
                        <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                        <label class="custom-control-label blabel" for="defaultUnchecked"><b>{{ __('I accept terms & conditions') }}.</b></label>
                    </div>
                    @endif
        	        <button class="btn btn-sm theme-bg btn-primary btn-block" style="font-size: 13px">{{ __('Join Now') }}</button>
        	    </form>
              </div>
              @endif
            </div>
            <div class="justify-content-center" id="loader" style="display: none">
              <div class="spinner-border theme-color" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>
          </div>
          @if(empty($forJoining) && !authUser(true))
          @php $shop = $_GET['shop']; @endphp
          <div class="modal-footer">
            <a href="https://{{$shop}}/account/register" target="_top" class="btn btn-danger waves-effect btn-block theme-bg mb-2">{{ __('Create Store Account') }}</a>
            <a href="https://{{$shop}}/account" target="_top" class="login-href btn btn-danger waves-effect btn-block theme-bg">{{ __('Log In') }}
            </a>
            <!--<a href="{{ route('customer.learnmore') }}" class="float-right action">Learn more-->
            <!--</a>-->
          </div>
          @endif
        </div>
        <!--/.Content-->
      </div>
    </div>
  </div>

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="{{ asset('customer/js/jquery-3.3.1.min.js') }}"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ asset('customer/js/popper.min.js') }}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ asset('customer/js/mdb.js') }}"></script>
  <script type="text/javascript">
    function handleAJAX(url, params = '')
    {
      $('#loader').show();
      $.ajax({
        url: url + parent.cus_attached_params + params, 
        success: function(result){
          $("#page-content").html(result);
          $('#loader').hide();
          $('#ajax-error').fadeOut();
        },
        error: function(){
          $('#ajax-error').fadeIn();
          setTimeout(function(){
            $('#ajax-error').fadeIn();
          }, 5000);
          $('#loader').hide();
        }
      });
    }
    $(document).on('click', 'a.action', function(e){
      e.preventDefault();
      var url = $(this).attr('href');
      handleAJAX(url);
    });
    $('.reward-toggler').click(function(){
        $('#shoployal-frame', window.parent.document).css({"height":"100%", "width": "300px"});
    });
    $(document).on('click', 'button.close-modal', function(){
      $('#shoployal-frame', window.parent.document).css({"height":"100px", "width": "240px"});
    });
    @if(authUser(true))
    handleAJAX("{{ route('customer.dashboard') }}");
    @elseif(empty($forJoining))
    handleAJAX("{{ route('customer.index') }}");
    @endif
    @if(!empty($joined))
    setTimeout(function(){
      $('#joining').fadeIn();
    }, 5000);
    @endif
    @if(authUser(true))
    @if(!empty($discount))
    function autoApplyDiscount()
    {
        $.ajax({
            url: "https://{{ $_GET['shop'] }}/discount/{{ urlencode($discount->code) }}",
            error: function(res){
                if(res.status != 400)
                    setTimeout(autoApplyDiscount, 5000);
            }
        });
    }
    autoApplyDiscount();
    @endif
    @endif
    @if(!empty($forJoining))
    $('#joining-form').submit(function(e){
        e.preventDefault();
        handleAJAX($(this).attr('action'), '&barcode=');
    });
    @endif
  </script>
</body>

</html>