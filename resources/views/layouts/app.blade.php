<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Shopify Application | ShopLoyal</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('shop/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('shop/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('shop/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('shop/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

    <link href="{{ asset('shop/plugins/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('shop/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('shop/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('shop/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('shop/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ asset('shop/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('shop/css/themes/theme-blue.css') }}" rel="stylesheet" />
    @stack('styles')
    <style type="text/css">
        body, .sidebar{
            font-family: "Quicksand" !important;
        }
        .navbar-nav .dropdown-menu{
            margin-top: 0px !important;
        }
        .user-helper-dropdown{
            box-shadow: none;
            margin-top: 23px;
        }
        .nav-icon{
            cursor: pointer;
            color: #fff;
            font-style: 26px;
        }
        .sidebar{
            padding-top: 20px;
        }
        .btn:not(.btn-link):not(.btn-circle) span {
            top: 0;
        }
        .bootstrap-select.form-control{
            border: 1px solid #ccc;
            border-radius: 4px !important;
        }
        .input-group input[type="text"], .input-group .form-control {
            border: 1px solid #ccc;
            padding-left: 12px;
        }
        .input-group .input-group-addon {
            padding-left: 12px;
        }
        .input-group .input-group-addon.with-border {
            border: 1px solid #ccc;
        }
        .dtp > .dtp-content > .dtp-date-view > header.dtp-header{
            background: #0D81DF;
        }
        .dtp div.dtp-date, .dtp div.dtp-time{
            background: #2196F3;
        }
        .dtp table.dtp-picker-days tr > td > a, .dtp .dtp-picker-time > a{
            border-radius: 0px !important;
        }
        .dtp table.dtp-picker-days tr td a.selected{
            background: #2196F3;
        }
        .horizon-prev , .horizon-next {
            box-shadow: none !important;
            padding: 3px 5px;
            font-weight: bold;
            border-radius: 0px !important;
        }
        section.content{
            margin-top: 90px;
        }
        .select2-container{
            width: 100% !important;
        }
        .dropify-message p{
            text-align: center;
        }

        .sidebar .menu .list li {
            margin: 0px 15px
        }
        .sidebar .menu .list a{
            color: inherit !important
        }
        .list .nav-li.active {
            background-color: #512F6C !important;
        }
        .list .nav-li.active i {
            color: #fff !important
        }
        .list .nav-li.active span {
            color: #fff !important
        }
        .list .nav-li.active a {
            color: #fff !important;
        }
        .bg-green{background-color: #512F6C !important;}
        .col-green{color: #512F6C !important;}
        .sidebar .menu{height: auto;}
        .sidebar .menu .list a span{text-transform: uppercase;font-size: 12px;}
        .sidebar .menu .list .ml-menu span{font-size: 12px;font-weight: bold;}
        .list .active a {box-shadow: 1px 2px 10px #ccc;}
        .block-header{margin-bottom: 30px}
        .card .header h2{font-weight: bold;}
        form .btn{font-weight: bold;padding: 10px 30px;background-color: #512F6C !important;}
        .navbar{background:#fff;box-shadow: 0 1px 15px rgba(0, 0, 0, 0.3);}
        .switch label input[type="checkbox"]:checked + .lever.switch-col-white {background-color: rgba(41, 47, 108, 0.7)};
        .dataTables_wrapper .select2-container{width: 64px;}
        .dataTables_wrapper .paginate, .dataTables_wrapper .dataTables_filter{float: right; margin: 0px;}
        .card .body .col-xs-12, .card .body .col-sm-12, .card .body .col-md-12, .card .body .col-lg-12{margin-bottom: 0px;}
        @media (max-width: 768px) { .user-helper-dropdown{margin: 5px 0px;} .navbar-nav.navbar-right{text-align: center;}}
    </style>
</head>
<body class="theme-white">
    <div class="page-loader-wrapper" id="loader" style="background: rgba(0,0,0,.7)">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-white">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p style="color: #fff">Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" style="color: #512F6C"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand text-center" style="color:#512F6C;" href="{{ route('shop.dashboard') }}"><b><i>Shoployal</i></b></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Notifications -->
                    <li class="dropdown">
                        <div class="btn-group user-helper-dropdown">
                            <div class="switch">
                                <label style="font-weight: bold;font-size: 14px;color: #512F6C">OFF<input type="checkbox" @if(userShop()->status) checked="" @endif id="shop-status"><span class="lever switch-col-white"></span>ON</label>
                            </div>
                        </div>
                    </li>
                    <!-- #END# Notifications -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    @include('components.sidebar')
    <section class="content">
        <div class="container-fluid" id="page-content">

        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="{{ asset('shop/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('shop/plugins/bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('shop/plugins/momentjs/moment.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('shop/plugins/dropify/dist/js/dropify.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('shop/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('shop/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <script src="{{ asset('shop/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('shop/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('shop/plugins/node-waves/waves.js') }}"></script>
    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('shop/plugins/jquery-countto/jquery.countTo.js') }}"></script>
    <!-- Morris Plugin Js -->
    <script src="{{ asset('shop/plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('shop/plugins/morrisjs/morris.js') }}"></script>

    <!-- Custom Js -->

    <!-- Demo Js -->
    <script src="{{ asset('shop/js/admin.js') }}"></script>
    <script src="{{ asset('shop/plugins/bootstrap-toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dashboard-ajax.js') }}"></script>
    @stack('scripts')
    <script type="text/javascript">
          $(window).ready(function(){handleAJAX(window.location.href)});
          function initPlugins()
          {
              if($('.summernote').length)
                  $('.summernote').summernote();
              if($('.dropify').length)
                  $('.dropify').dropify();
              if($('.dataTable').length)
                  $('.dataTable').DataTable({'ordering': false});
              if($('.timepicker').length)
                  $('.timepicker').timepicker({showMeridian:!1});

              $('select:not(.ms)').select2();

              $('.datepicker').bootstrapMaterialDatePicker({
                    format: 'YYYY-MM-DD HH:mm',
                    clearButton: true,
                    weekStart: 1
                });
              $('.count-to').countTo();
          }

        @if(session('success'))
        toastr.success('{{ session("success") }}');
        @elseif(session('error'))
        toastr.error('{{ session("error") }}');
        @endif

        $('#shop-status').change(function(){
            if($(this).is(':checked'))
                handleAJAX("{{ route('enable') }}", false, 'action');
            else
                handleAJAX("{{ route('disable') }}", false, 'action');
        });
        $(document).on('click', '.horizon-prev', function() {
            $(this).closest('.card').find('.table-responsive').animate({
              scrollLeft: "-=700px"
            }, "slow");
        });
        $(document).on('click', '.horizon-next', function() {
            $(this).closest('.card').find('.table-responsive').animate({
              scrollLeft: "+=700px"
            }, "slow");
        });
    </script>
</body>
