<!DOCTYPE html>
<html>
<head>
    <style>
        body{
            font-weight:normal;
            padding: 30px;
            float: left;
        }
        div{
            width: 100%;
            float: left;
            box-sizing: border-box;
        }
        .container{
            font-family:'Roboto';
            font-size: 16px;
            background-color: #d5f7f9;
        }
        .header img{
            width: 100%;
        }
        h1,h4,.fbold, b, .link{
            font-weight: 700;
        }
        .content{
            padding: 10px 30px;
        }
        .discounts{
            padding: 30px;
        }
        .desc-area{
            padding-top: 10px;
            padding-bottom: 30px;
        }
        .desc-inner{
            padding-left: 23px;
        }
        .after-desc1 , .after-desc2{
            padding: 15px 0px;
        }
        .link{
            color: black;
        }
        .footer-img{
            margin-top: -15px;
        }
        .footer-img img{
            float: right;
            width: 200px;
        }
        @media(max-width: 768px){
            body{
                padding: 30px 0px;
                font-size: 15px;
                line-height: 24px;
            }
            .content {
                padding: 10px;
            }
            .heading{
                font-size: 20px;
            }
            .footer-img img{
                width: 150px;
            }
            .discounts{
                padding: 15px;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <img src="{{ asset('customer/img/hand-img.png') }}" />
        </div>
        <div class="content">
            @php error_reporting(0); $customer = $data['customer']; @endphp
            <h1 class="heading fbold">1. REGISZTRÁCIÓT KÖVETŐEN</h1>
            <h4 class="c-name">Kedves {{ $customer->name }},</h4>
            <p class="subheading">Üdvözlünk a Játéksziget hűségklubjában!</p>
            <p>
                Mostantól minden egyes vásárlás után, vásárlásod összegét rögzítjük. Személyes hűségkártya forint egyenleged minden elköltött forinttal nő. A hűségkártya egyenleg nem elkölthető egyenleg, nem levásárolható, hanem
                
                ahogy gyarapszik, úgy nő a kedvezmény mértéke.
                Amint egyenleged eléri az alábbi sávok valamelyikét*, a sávhoz tartozó
                
                %-os kedvezményre leszel jogosult a programban való regisztrációtól számított 365 napig! Az összeg automatikusan nullázódik 365 nap után.
            </p>
            <div class="discounts">
                <b>1.</b> 30.000 Ft-tól 59.999 Ft-ig <b>5%</b><br />
                <b>2.</b> 60.000 Ft-tól 99.999 Ft-ig <b>7%</b><br />
                <b>3.</b> 100.000 Ft-tól <b>10%</b>
            </div>
            <p>*a kedvezmény akciós termékekre nem érvényes!</p>
            <h3 class="fbold heading2">Kedvezményedet az alábbi módokon érvényesítheted:</h3>
            <div class="desc-area">
                <b>1.</b> A Játéksziget üzletekben hűségkártyád segítségével, amit vásárláskor az
                üzletben vehetsz át<br />
                <div class="desc-inner">
                    <b>•</b> a kártyán található vonalkód alapján rendszerünk beazonosítja az aktuális kedvezményedet
                </div>
                <b>2.</b> www.jateksziget.hu webáruházban<br />
                <div class="desc-inner">
                    <b>•</b> A fiókodba történő belépést követően a képernyő jobb alsó sarkában
                    találod meg, hogy milyen kedvezmény sávba tartozol.<br />
                    <b>•</b> A rendelés leadásakor a rendszer jogosultságod függvényében automatikusan levonja a kedvezményt<br />
                    <b>•</b> Személyes, utánvétes csomagátvétel esetén automatikusan a kedvezménnyel csökkentett értéket kell fizetned. A kártyádra ez esetben nem
                    lesz szükség.<br />
                </div>
            </div>
            <div class="after-desc1">
                Használd ki a Játéksziget hűségklub adta rendkívüli lehetőséget! <b>Klubtagként minden Lego terméket -10% kedvezménnyel vásárolhatsz meg</b> tagságod
                érvényességi ideje alatt!
            </div>
            <div class="after-desc2">
                Amennyiben további kérdésed adódik, olvasd el a programmal kapcsolatos
                „Kérdéseket és válaszokat” mely az alábbi linken érhető el: <a class="link" href="https://jateksziget.hu/apps/help-center">https://jateksziget.hu/apps/help-center</a>
            </div>
            <p class="last-desc">
                További kellemes napot, játékos időtöltést kívánunk! <br />
                A Játéksziget csapata
            </p>
            <div class="footer-img">
                <img src="{{ asset('customer/img/jatek-img.png') }}">
            </div>
        </div>
    </div>
</body>
</html>