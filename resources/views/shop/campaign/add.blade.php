<style type="text/css">
	.p-img, .p-title{
		display: inline-block;
	}
	.p-img img{
		height: 35px;
		width: 35px;
	}
	.p-title{
		padding-left: 10px;	
	}
	.filter-option img{
		height: 19px;
		width: 20px;
	}
	.wizard .content{
		overflow: visible;
		min-height: auto;
	}
</style>
@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b>Add Campaign</b></br></h3>
</div>
<div class="row clearfix">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="card">
			<div class="header bg-green">
                <h2>Campaign Setup</h2>
            </div>
            <div class="body">
            	<form method="post" action="{{ route('save.campaign') }}" id="wizard_with_validation">
            		@csrf
					<h3>Campaign Info</h3>
					<fieldset>
						<label>{{ TR('Campaign') }} Name</label>
	                    <div class="m-b-15">
	                    	<input type="text" class="form-control" placeholder="{{ TR('Campaign') }} Name" name="name" />
	                    </div>
	                    <label>Starts At</label>
	                    <div class="m-b-15">
	                        <input type="text" class="form-control datepicker" placeholder="{{ TR('Campaign') }} Starting Date" name="starts_at" />
	                    </div>
	                    <label>Ends At <small>(Optional)</small></label>
	                    <div class="">
	                      	<input type="text" class="form-control datepicker" placeholder="{{ TR('Campaign') }} Ending Date" name="ends_at" />
	                    </div>
					</fieldset>
					<h3>Set Target</h3>
					<fieldset>
						<label>Customer Level</label>
						<div class="m-b-15">
							<select class="form-control" name="target_level">
	                        	<option value="" selected="">All</option>
	                        	@foreach($levels as $lvl)
	                        	<option value="{{ $lvl->id }}">{{ $lvl->title }}</option>
	                        	@endforeach
	                        </select>
						</div>
                        <label>Customer Age</label>
                        <div class="m-b-15">
                        	<div class="input-daterange input-group">
	                            <input type="text" class="form-control" placeholder="Age Start" name="target_age_from" value="3">
	                            <span class="input-group-addon">to</span>
	                            <input type="text" class="form-control" placeholder="Age End" name="target_age_to" value="80">
	                    	</div>
                        </div>
                    	<label>Customer Gender</label>
	                	<div class="m-b-15">
	                		<select class="form-control" name="target_gender">
	                        	<option value="" selected="">Both</option>
	                        	<option value="male">Male</option>
	                        	<option value="female">Female</option>
	                        </select>
	                	</div>
                        <div>
                        	<input type="checkbox" id="if-birthday" class="filled-in chk-col-green" checked="" value="1" name="if_birthday">
	                    	<label for="if-birthday">On Customer's Birthday</label>
                        </div>
					</fieldset>
					<h3>Discount Rule</h3>
					<fieldset>
						<label>{{ TR('Required Points') }}</label>
	                    <div class="m-b-15">
	                        <input type="text" class="form-control " placeholder="Required Points" name="points" />
	                    </div>
	                    <label>{{ TR('Discount Value') }}</label>
	                    <div class="m-b-15">
	                       <input type="text" class="form-control" placeholder="Discount Value" name="discount" />
	                    </div>
	                	<label>{{ TR('Discount Type') }}</label>
	                    <div class="">
	                        <select class="form-control" name="discount_type">
	                            <option value="1">{{ TR('Percentage') }}</option>
	                            <option value="2">{{ TR('Fixed') }}</option>
	                        </select>
	                    </div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script src="{{ asset('shop/plugins/jquery-steps/jquery.steps.js') }}"></script>
<script src="{{ asset('shop/js/pages/forms/form-wizard.js') }}"></script>