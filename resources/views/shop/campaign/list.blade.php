@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b>All Campaigns</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Campaigns
                    <a href="{{ route('add.campaign') }}" class="btn btn-default waves-effect pull-right m-t--5 font-bold text-uppercase call-url">
                        Add New
                    </a>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ TR('Campaign') }} Name</th>
                            <th>Starts At</th>
                            <th>Ends At</th>
                            <th>T.Level</th>
                            <th>T.Age</th>
                            <th>T.Gender</th>
                            <th>On BD</th>
                            <th>Offers</th>
                            <th>Discount</th>
                            <th>Code</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($campaigns as $c)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                            	<a href="{{ route('edit.campaign', $c->id) }}" class="call-url">
                            		{{ $c->name }}
                            	</a>
                            </td>
                            <td>{{ date('M d, Y', strtotime($c->starts_at)) }}</td>
                            <td>{{ date('M d, Y', strtotime($c->ends_at)) }}</td>
                            <td>{{ $c->customerLevelPoint->title ?: 'All' }}</td>
                            <td>
                                {{ $c->target_age_from }} <b>to</b> {{ $c->target_age_to }}
                            </td>
                            <td>{{ $c->target_gender ?: 'All' }}</td>
                            <td>{{ $c->if_birthday ? 'Yes' : 'No' }}</td>
                            <td>
                                {{ $c->offers }}
                            </td>
                            <td>{{ $c->discount }} <small>{{ $c->discount_type == 1 ? 'Fixed' : 'Percent' }}</small></td>
                            <td>{{ $c->code }}</td>
                            <td>
                                @if($c->status == 1)
                                <span class="label label-success">Active</span>
                                @else
                                <span class="label label-danger">Disabled</span>
                                @endif
                            </td>
                            <td>
                                @if($c->status)
                                <a href="{{ route('update.campaign.status', [$c->id, 0]) }}" class="btn btn-warning waves-effect btn-sm action-url">
                                    <i class="material-icons">lock</i>
                                </a>
                                @else
                                <a href="{{ route('update.campaign.status', [$c->id, 1]) }}" class="btn btn-primary waves-effect btn-sm action-url">
                                    <i class="material-icons">lock_open</i>
                                </a>
                                @endif
                                <a href="{{ route('edit.campaign', $c->id) }}" class="btn btn-info waves-effect btn-sm call-url">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="{{ route('delete.campaign', $c->id) }}" class="btn btn-danger waves-effect btn-sm action-url">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
