@php error_reporting(0); @endphp
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-light-blue">
                <h2>
                    Availed items of <b>{{ $campaign->name }}</b>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Image</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($items as $i)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $i->title }}</td>
                            <td>{{ userShop()->currency }}{{ $i->price }}</td>
                            <td><img src="{{ $i->image }}" height="50" width="50"></td>
                            <td>
                                <a href="{{ route('item.availers', $i->id) }}" class="btn btn-danger waves-effect btn-sm">View Detail</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
