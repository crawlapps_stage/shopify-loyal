<div class="block-header text-center">
    <h3><b>Add Discount Rule</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('save.discount') }}">
		@csrf
        <input type="hidden" name="per" value="{{ $per }}">
	    <div class="col-md-8 col-md-offset-2">
	        <div class="card">
                <div class="header bg-green">
                    <h2>
                        {{ TR('Discount Rules') }}
                    </h2>
                </div>
	            <div class="body">
	                <div class="row clearfix">
	                    <div class="col-sm-12">
                            @if($per == 'campaign')
                            <label><b>Select Campaign</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id" data-live-search="true">
                                    @foreach($campaigns as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @elseif($per == 'event')
                            <label><b>Select Event</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id" data-live-search="true">
                                    @foreach($events as $e)
                                    <option value="{{ $e->id }}">{{ $e->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @else
                            <label><b>Select Customer Level</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id" >
                                    @foreach($levels as $l)
                                    <option value="{{ $l->id }}">{{ $l->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <label>{{ TR('Discount Code') }}</label>
	                        <div class="m-b-15">
	                           <input type="text" class="form-control" placeholder="Discount Code" name="coupon_code" />
	                        </div>
                            <label>{{ TR('Discount Type') }}</label>
                            <div class="m-b-15">
                                <select class="form-control" name="discount_type">
                                    <option value="1">{{ TR('Percentage') }}</option>
                                    <option value="2">{{ TR('Fixed') }}</option>
                                    <option value="3">{{ TR('Free Shipping') }}</option>
                                </select>
                            </div>
	                        <label class="if-discount">{{ TR('Discount Value') }}</label>
	                        <div class="m-b-15 if-discount">
	                           <input type="text" class="form-control" placeholder="Discount Value" name="discount" />
	                        </div>
	                        @if($per != 'general')
                            <label>{{ TR('Required Points') }}</label>
                            <div class="m-b-15">
                                <div class="form-line">
                                    <input type="number" class="form-control " placeholder="Required Points" name="points" />
                                </div>
                            </div>
                            @endif
                            <button class="btn btn-sucess btn-lg waves-effect bg-green">Save</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </form>
</div>
<script type="text/javascript">
    $('select[name="discount_type"]').change(function(){
        if($(this).val() != "3")
            $('.if-discount').show();
        else
            $('.if-discount').hide();
    });
</script>
