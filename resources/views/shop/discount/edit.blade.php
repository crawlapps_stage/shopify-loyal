<style type="text/css">.if-discount{display: none;}</style>
<div class="block-header text-center">
    <h3><b>Update Discount Rule</b></br></h3>
</div>
<div class="row clearfix">
    <form method="post" action="{{ route('save.discount', $discount->id) }}">
        @csrf
        <div class="col-md-8 col-md-offset-2">
            <input type="hidden" name="per" value="{{ $per }}">
            <div class="card">
                <div class="header bg-green">
                    <h2>
                        <b>{{ TR('Discount Rules') }}</b>
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            @if($discount->based_on == 1)
                            <label><b>Select Campaign</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id" data-live-search="true">
                                    @foreach($campaigns as $c)
                                    <option value="{{ $c->id }}" @if($discount->based_on_id == $c->id) selected @endif>{{ $c->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @elseif($discount->based_on == 2)
                            <label><b>Select Event</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id" data-live-search="true">
                                    @foreach($events as $e)
                                    <option value="{{ $e->id }}" @if($discount->based_on_id == $e->id) selected @endif>{{ $e->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @else
                            <label><b>Select Customer Level</b></label>
                            <div class="m-b-15">
                                <select class="form-control selectpicker" name="based_on_id">
                                    @foreach($levels as $l)
                                    <option value="{{ $l->id }}" @if($discount->based_on_id == $l->id) selected @endif>{{ $l->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <label>{{ TR('Discount Code') }}</label>
	                        <div class="m-b-15">
	                           <input type="text" class="form-control" placeholder="Discount Code" name="coupon_code" value="{{ $discount->code }}" />
	                        </div>
                            <label>{{ TR('Discount Type') }}</label>
                            <div class="m-b-15">
                                <select class="form-control" name="discount_type">
                                    <option value="1" @if($discount->discount_type == 1) selected @endif>{{ TR('Percentage') }}</option>
                                    <option value="2" @if($discount->discount_type == 2) selected @endif>{{ TR('Fixed') }}</option>
                                    <option value="3" @if($discount->discount_type == 3) selected @endif>{{ TR('Free Shipping') }}</option>
                                </select>
                            </div>
                            <label class="if-discount">{{ TR('Discount Value') }}</label>
                            <div class=" if-discount">
                                <input type="text" class="form-control" placeholder="Discount Value" name="discount" value="{{ $discount->discount }}" />
                            </div>
                            @if($per != 'general')
                            <label>{{ TR('Required Points') }}</label>
                            <div class="m-b-15">
                                <div class="form-line">
                                    <input type="number" class="form-control " placeholder="Required Points" name="points" value="{{ $discount->points }}" />
                                </div>
                            </div>
                            @endif
                            <button class="btn btn-sucess btn-lg waves-effect  bg-green">Save Changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $('select[name="discount_type"]').change(function(){
        if($(this).val() != "3")
            $('.if-discount').show();
        else
            $('.if-discount').hide();
    });
</script>
