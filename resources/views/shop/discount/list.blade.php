<div class="block-header text-center">
    <h3><b>Discount Rules</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    {{ TR('Discount Rules') }}
                    <a href="{{ route('add.discount', $per) }}" class="btn btn-default waves-effect pull-right m-t--5 font-bold text-uppercase call-url">
                        Add New
                    </a>
                </h2> 
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                @if($per == 'campaign')
                                Campaign Name
                                @elseif($per == 'event')
                                Event Name
                                @else
                                Customer Level
                                @endif
                            </th>
                            @if($per != 'general')
                            <th>{{ TR('Required Points') }}</th>
                            @endif
                            <th>{{ TR('Discount Type') }}</th>
                            <th>{{ TR('Discount Value') }}</th>
                            <th>{{ TR('Discount Code') }}</th>
                            <th>{{ TR('Status') }}</th>
                            <th>{{ TR('Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($discounts as $d)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                            	@if($per == 'campaign')
                                {{ $d->campaign->name }}
                                @elseif($per == 'event')
                                {{ $d->event->name }}
                                @else
                                {{ $d->customerLevelPoint->title }}
                                @endif
                            </td>
                            @if($per != 'general')
                            <td>{{ $d->points }}</td>
                            @endif
                            <td>
                                @if($d->discount_type == 1)
                                {{ TR('Percentage') }}
                                @elseif($d->discount_type == 2)
                                {{ TR('Fixed') }}
                                @else
                                {{ TR('Free Shipping') }}
                                @endif
                            </td>
                            <td>
                                @if($d->discount_type == 3)
                                {{ TR('Free Shipping') }}
                                @else
                                {{ $d->discount }}
                                @endif
                            </td>
                            <td>
                                {{ $d->code }}
                            </td>
                            <td>
                                @if($d->status)
                                <span class="label label-success">Active</span>
                                @else
                                <span class="label label-success">De-Active</span>
                                @endif
                            </td>
                            <td>
                                @if($d->status)
                                <a href="{{ route('update.discount.status', [$d->id, 0]) }}" class="btn btn-warning waves-effect btn-sm action-url">
                                    <i class="material-icons">lock</i>
                                </a>
                                @else
                                <a href="{{ route('update.discount.status', [$d->id, 1]) }}" class="btn btn-primary waves-effect btn-sm action-url">
                                    <i class="material-icons">lock_open</i>
                                </a>
                                @endif
                                <a href="{{ route('edit.discount', [$per, $d->id]) }}" class="btn btn-info waves-effect btn-sm call-url">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="{{ route('delete.discount', [$per, $d->id]) }}" class="btn btn-danger waves-effect btn-sm action-url">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
