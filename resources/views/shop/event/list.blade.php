<div class="block-header text-center">
    <h3><b>All Events</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Events
                    <a href="{{ route('add.event') }}" class="btn btn-default waves-effect pull-right m-t--5 font-bold text-uppercase call-url">
                        Add New
                    </a>
                </h2> 
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Event Name</th>
                            <th>Starts At</th>
                            <th>Ends At</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($events as $e)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                            	<a href="{{ route('edit.event', $e->id) }}" class="call-url">
                            		{{ $e->name }}
                            	</a>
                            </td>
                            <td>{{ date('M d, Y', strtotime($e->starts_at)) }}</td>
                            <td>{{ date('M d, Y', strtotime($e->ends_at)) }}</td>
                            <td>
                                @if($e->status)
                                <span class="label label-success">Active</span>
                                @else
                                <span class="label label-danger">De-Active</span>
                                @endif
                            </td>
                            <td>
                                @if($e->status)
                                <a href="{{ route('update.event.status', [$e->id, 0]) }}" class="btn btn-warning waves-effect btn-sm action-url">
                                    <i class="material-icons">lock</i>
                                </a>
                                @else
                                <a href="{{ route('update.event.status', [$e->id, 1]) }}" class="btn btn-primary waves-effect btn-sm action-url">
                                    <i class="material-icons">lock_open</i>
                                </a>
                                @endif
                                <a href="{{ route('edit.event', $e->id) }}" class="btn btn-info waves-effect btn-sm call-url">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="{{ route('delete.event', $e->id) }}" class="btn btn-danger waves-effect btn-sm action-url">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
