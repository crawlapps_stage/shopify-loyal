<div class="block-header text-center">
    <h3><b>Add Event Details</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('save.event') }}">
		@csrf
	    <div class="col-md-6">
	        <div class="card">
	        	<div class="header bg-green">
                    <h2>
                        <b>Event Details</b>
                    </h2>
                </div>
	            <div class="body">
	                <div class="row clearfix">
	                    <div class="col-sm-12">
	                    	<label>Event Name</label>
	                        <div class="m-b-15">
	                                <input type="text" class="form-control" placeholder="Event Name" name="name" />
	                        </div>
	                        <label>Starts At</label>
	                        <div class="m-b-15">
	                                <input type="text" class="form-control datepicker" placeholder="Event Starting Date" name="starts_at" />
	                        </div>
	                        <label>Ends At <small>(Optional)</small></label>
	                        <div class="">
	                                <input type="text" class="form-control datepicker" placeholder="Event Ending Date" name="ends_at" />
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="col-md-6">
	        <div class="card">
                <div class="header bg-green">
                    <h2>
                    	<b>Extra Points</b>
                    </h2> 
                </div>
                <div class="body table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Earning Action</th>
                                <th>Extra Points</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($earningRules as $ea)
                            <tr>
                                <td>{{ $ea->earningAction->name }}</td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Extra Points" name="points[]">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
	    </div>
	    <div class="col-md-12">
	    	<button class="btn btn-sucess btn-lg waves-effect bg-green">Save</button>
	    </div>
    </form>
</div>