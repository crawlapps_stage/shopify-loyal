<div class="block-header text-center">
    <h3><b>Display Setting</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('update.display') }}">
		@csrf
	    <div class="col-md-8 col-md-offset-2">
	        <div class="card">
                <div class="header bg-green">
                    <h2>
                        @lang('Update Display')
                    </h2>
                </div>
	            <div class="body">
	                <div class="row clearfix">
	                    <div class="col-sm-12">
                            <label>Button Text</label>
                            <div class="m-b-15">
                                <input type="text" name="button_text" class="form-control" placeholder="Button Text" value="{{ $shop->button_text }}">
                            </div>
                            <label>Header Text</label>
                            <div class="m-b-15">
                                <input type="text" name="header_text" class="form-control" placeholder="Header Text" value="{{ $shop->header_text }}">
                            </div>
                            <label>Background Color <small>(HEX color code)</small></label>
                            <div class="m-b-15">
                                <input type="text" name="background_color" class="form-control" placeholder="Paste HEX color code" value="{{ $shop->background_color }}">
                            </div>
                            <button class="btn btn-success">Update Display</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    </form>
</div>