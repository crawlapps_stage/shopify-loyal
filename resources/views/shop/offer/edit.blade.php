<div class="block-header text-center">
    <h3><b>Update Offer</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('save.offer', $offer->id) }}" enctype="multipart/form-data">
		@csrf
	    <div class="col-md-8 col-md-offset-2">
	        <div class="card">
	        	<div class="header bg-green">
	                <h2>UPDATED OFFER</h2>
	            </div>
	            <div class="body">
                    <div class="m-b-15">
                    	<label>Selected Campaign</label>
                        <input type="text" readonly="" value="{{ $campaign->name }}" class="form-control">
                        <input type="hidden" name="campaign" value="{{ $campaign->id }}">
                    </div>
                    <hr>
                    <div class="m-b-15">
                    	<label>Image</label>
                    	<input type="file" name="image" class="dropify" @if($offer->imageUrl) data-default-file="{{ $offer->imageUrl }}" @endif>
                    </div>
                    <div class="m-b-15">
                    	<label>Name</label>
                    	<input type="text" name="name" class="form-control" value="{{ $offer->name }}">
                    </div>
                    <div class="m-b-15">
                        <label>Details</label>
                        <textarea class="form-control" name="details" rows="4">{{ $offer->details }}</textarea>
                    </div>
                    @if(sizeof($offerCategories) > 0)
                    <div class="m-b-15">
                    	<h2 class="card-inside-title">Offer Categories</h2>
                    	<div class="row">
	                    	@foreach($offerCategories as $oc)
	                    	<div class="col-md-4">
		                    	<input type="checkbox" @if(strpos($offer->offerCategories, (string)$oc->item_id) !== false) checked="" @endif value="{{ $oc->item_id }}" id="basic_checkbox_{{ $loop->iteration }}" class="filled-in" name="offer_categories[]" />
		                        <label for="basic_checkbox_{{ $loop->iteration }}">{{ $oc->name }}</label>
	                        </div>
	                        @if($loop->iteration % 3 == 0)
	                    	</div><div class="row">
	                        @endif
	                    	@endforeach
                    	</div>
                    </div>
                    @endif
                    @if(sizeof($offerHubs) > 0)
                    <div class="m-b-15">
                    	<h2 class="card-inside-title">Offer Hubs</h2>
                    	<div class="row">
	                    	@foreach($offerHubs as $oh)
	                    	<div class="col-md-4">
		                    	<input type="checkbox" @if(strpos($offer->offerHubs, $oh->key) !== false) checked="" @endif value="{{ $oh->key }}" id="basic_checkbox_{{ $loop->iteration }}" class="filled-in" name="hubs[]" />
		                        <label for="basic_checkbox_{{ $loop->iteration }}">{{ $oh->name }}</label>
	                        </div>
	                        @if($loop->iteration % 3 == 0)
	                    	</div><div class="row">
	                        @endif
	                    	@endforeach
                    	</div>
                    </div>
                    @endif
                    <button class="btn btn-primary btn-lg waves-effect">Create Offer</button>
	            </div>
	        </div>
	    </div>
    </form>
</div>