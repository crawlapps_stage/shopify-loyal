@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b>All Offers</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Offers
                    <a href="{{ route('add.offer') }}" class="btn btn-default waves-effect pull-right m-t--5 font-bold text-uppercase call-url">
                        Add New
                    </a>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Campaign</th>
                            <th>Details</th>
                            <th>Image</th>
                            <th>Point Value</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($offers as $o)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                            	<a href="{{ route('edit.offer', $o->id) }}" class="call-url">
                            		{{ $o->name }}
                            	</a>
                            </td>
                            <td>{{ App\Campaign::find($o->campaign_id)->name }}</td>
                            <td>{{ $o->details }}</td>
                            <td>
                                @if($o->imageUrl)
                                <img src="{{ $o->imageUrl }}" width="50" height="50">
                                @endif
                            </td>
                            <td>{{ $o->offer_point_value ?: 0 }}</td>
                            <td>
                                @if($o->sent)
                                <span class="label label-primary">Sent</span>
                                @elseif($o->availed)
                                <span class="label label-success">Availed</span>
                                @else
                                <span class="label label-info">Pending</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('edit.offer', $o->id) }}" class="btn btn-info waves-effect btn-sm call-url">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="{{ route('delete.offer', $o->id) }}" class="btn btn-danger waves-effect btn-sm action-url">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
