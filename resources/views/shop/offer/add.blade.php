<div class="block-header text-center">
    <h3><b>Add Offer</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('save.offer') }}" enctype="multipart/form-data">
		@csrf
	    <div class="col-md-8 col-md-offset-2">
	        <div class="card">
	        	<div class="header bg-green">
	                <h2>CREATE OFFER</h2>
	            </div>
	            <div class="body">
                    <div class="m-b-15">
                    	<label>Select Campaign</label>
                        <select class="form-control" name="campaign">
                        	@foreach($campaigns as $c)
                        	<option value="{{ $c->id }}">{{ $c->name }}</option>
                        	@endforeach
                        </select>
                    </div>
                    <hr>
                    <div class="m-b-15">
                    	<label>Image</label>
                    	<input type="file" name="image" class="dropify">
                    </div>
                    <div class="m-b-15">
                    	<label>Name</label>
                    	<input type="text" name="name" class="form-control">
                    </div>
                    <div class="m-b-15">
                    	<label>Details</label>
                    	<textarea class="form-control" name="details" rows="4"></textarea>
                    </div>
                    <div class="m-b-15">
                    	<h2 class="card-inside-title">Offer Categories</h2>
                    	<div class="row">
	                    	@foreach($offerCategories as $oc)
	                    	<div class="col-md-4">
		                    	<input type="checkbox" value="{{ $oc->item_id }}" id="basic_checkbox_{{ $loop->iteration }}" class="filled-in" name="offer_categories[]" />
		                        <label for="basic_checkbox_{{ $loop->iteration }}">{{ $oc->name }}</label>
	                        </div>
	                        @if($loop->iteration % 3 == 0)
	                    	</div><div class="row">
	                        @endif
	                    	@endforeach
                    	</div>
                    </div>
                    @if(sizeof($offerHubs) > 0)
                    <div class="m-b-15">
                    	<h2 class="card-inside-title">Offer Hubs</h2>
                    	<div class="row">
	                    	@foreach($offerHubs as $oh)
	                    	<div class="col-md-4">
		                    	<input type="checkbox" value="{{ $oh->key }}" id="basic_checkbox_{{ $loop->iteration }}" class="filled-in" name="hubs[]" />
		                        <label for="basic_checkbox_{{ $loop->iteration }}">{{ $oh->name }}</label>
	                        </div>
	                        @if($loop->iteration % 3 == 0)
	                    	</div><div class="row">
	                        @endif
	                    	@endforeach
                    	</div>
                    </div>
                    @endif
                    <button class="btn btn-primary btn-lg waves-effect">Create Offer</button>
	            </div>
	        </div>
	    </div>
    </form>
</div>