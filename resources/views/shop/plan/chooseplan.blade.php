@extends('layouts.app')
@section('title', 'Choose Subscription Plan | ShopLoyal')
@section('heading', 'CHOOSE SUBSCRIPTION PLAN')
@push('styles')
	<link href="{{ asset('shop/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
	<style type="text/css">
	body {
		background-color: #e9e9e9;
	}
		.container {
		  display: flex;
		  padding-top: 40px;
		}

		.col {
			margin-right: 10px;
		  width: 290px;
		  min-height: 200px;
		  text-align: center;
		  height: 100%;
		}

		.card {
		  height: 100%;
		  padding: 1rem;
		  margin-bottom: 1rem;
		  display: flex;
		  flex-direction: column;
		  justify-content: space-between;
		  box-shadow: 1px 1px 25px 0px rgba(143, 85, 85, 0.1);
		}

		.card-header {
		  height: 30%;
		  display: flex;
		  flex-direction: column;
		  justify-content: space-around;
		  margin-bottom: 2rem;
		}
		.card-header .title {
		  font-size: 1.5em;
		  font-weight: bold;
		  color: rgba(37, 36, 53, 0.9);
		  letter-spacing: 2px;
		}
		.card-header .title span {
		  color: #ff5a5f;
		}
		.card-header .price {
		  font-size: 2.5em;
		  color: #ff5a5f;
		  width: 40%;
		  margin: 0 auto;
		  padding: 10px;
		}
		.card-header .price span {
		  font-size: 0.5em;
		  color: inherit;
		}
		.card-header .description {
		  color: rgba(37, 36, 53, 0.7);
		}

		.features {
		  height: 70%;
		  text-align: left;
		  list-style: none;
		}
		.features li {
		  color: rgba(37, 36, 53, 0.7);
		  margin-bottom: 1rem;
		}

		.line {
		  width: 35%;
		  margin: 15px auto 0;
		  height: 3px;
		  background-color: rgba(255, 90, 95, 0.9);
		}

		.fa-check{
			color: #5ECD1A;
		}
		.fa-times{
			color: #FF6A6F;
		}
		.text-center{
			text-align: center;
		}
		.mb-3{
			margin-bottom: 3%;
		}
		.bold{
			font-weight: bold;
		}
		input,.StripeElement {
		  box-sizing: border-box;

		  height: 40px;

		  padding: 10px 12px;

		  border: 1px solid #ccc;
		  border-radius: 4px;
		  background-color: white;
		  transition: box-shadow 150ms ease;
		}

		.StripeElement--focus {
		  box-shadow: 0 1px 3px 0 #cfd7df;
		}

		.StripeElement--invalid {
		  border-color: #fa755a;
		}

		.StripeElement--webkit-autofill {
		  background-color: #fefde5 !important;
		}
		.alert{
		    padding: 6px 12px;
		    margin-bottom: 0px;
		    font-weight: bold;
		}
	</style>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('shop/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet">
@endpush
@section('content')
@php error_reporting(0); @endphp
<div class="row clearfix">
			<div class="container">
			  <div class="col">
			    <div class="card">
			      <div class="card-header">
			        <h2 class="title">Basic</h2>
			        <p class="price">
			          0
			          <span>$</span>
			        </p>
			        <span class="description">Basic Plan</span>
			        <div class="line"></div>
			      </div>

			      <ul class="features">
			        <li><i class="fa fa-check"></i> Loyalty Points System</li>
			        <li><i class="fa fa-check"></i> Etc Etc</li>
			        <li><i class="fa fa-times"></i> User App</li>
			        <li><i class="fa fa-times"></i> Offline Data</li>
			      </ul>
			      @if($shop->plan == 0 || $shop->plan > 1)
			      <a href="{{ route('go.free') }}" class="btn btn-success">Go Free</a>
			      @else
			      <div class="alert alert-success">
			      	Current Plan
			      </div>
			      @endif
			    </div>
			  </div>
			  <div class="col">
			    <div class="card">
			      <div class="card-header">
			        <h2 class="title">Silver</h2>
			        <p class="price">
			          50
			          <span>$</span>
			        </p>
			        <span class="description">Silver Plan</span>
			        <div class="line"></div>
			      </div>

			      <ul class="features">
			        <li><i class="fa fa-check"></i> Loyalty Points System</li>
			        <li><i class="fa fa-check"></i> Etc Etc</li>
			        <li><i class="fa fa-check"></i> User App</li>
			        <li><i class="fa fa-times"></i> Offline Data</li>
			      </ul>
			      @if($shop->plan != 2)
			      <a href="javascript:;" class="btn btn-success" data-target="#myModal" data-toggle="modal" data-plan="silver-plan" data-amount="50">Choose Plan</a>
			      @else
			      <div class="alert alert-success">
			      	Current Plan
			      </div>
			      @endif
			    </div>
			  </div>
			  <div class="col">
			    <div class="card">
			      <div class="card-header">
			        <h2 class="title">Golden</h2>
			        <p class="price">
			          100
			          <span>$</span>
			        </p>
			        <span class="description">Golden Plan</span>
			        <div class="line"></div>
			      </div>

			      <ul class="features">
			        <li><i class="fa fa-check"></i> Loyalty Points System</li>
			        <li><i class="fa fa-check"></i> Etc Etc</li>
			        <li><i class="fa fa-check"></i> User App</li>
			        <li><i class="fa fa-check"></i> Offline Data</li>
			      </ul>
			      @if($shop->plan != 3)
			      <a href="javascript:;" class="btn btn-success" data-target="#myModal" data-toggle="modal" data-plan="golden-plan" data-amount="100">Choose Plan</a>
			      @else
			      <div class="alert alert-success">
			      	Current Plan<br>
			      </div>
			      @endif
			    </div>
			  </div>
			</div>
		</div>

		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Enter Payment Details</h4>
		      </div>
		      <div class="modal-body">
		        <form action="{{ route('subscribe') }}" method="post" id="payment-form" onsubmit="return false;">
		          @csrf
		          <input type="hidden" name="plan" id="plan">
		          <div id="card-errors" role="alert"></div>
		          @if(is_null(userShop()->company_id))
		          <div class="form-group">
				  	<input type="text" class="form-control" name="company_id" id="company-id" placeholder="Enter company id (provided by ShopLoyal)">
				  </div>
				  @endif
				  <div class="form-group">
				  	<input type="text" class="form-control" name="holder_name" id="card-holder-name" placeholder="Enter card holder name">
				  </div>
				  <div class="form-group">
				  	<div id="card-element"></div>
				  </div>
				  <input type="hidden" name="payment_method" id="payment-method">
				  <button class="btn btn-success" id="card-button" data-secret="{{ $intent->client_secret }}">Submit Payment</button>
				</form>
		      </div>
		    </div>

	</div>
</div>
@endsection