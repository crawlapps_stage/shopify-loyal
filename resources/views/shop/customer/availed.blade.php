@php error_reporting(0); @endphp
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    <b>{{ $item->title }}</b> Availers
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer</th>
                            <th>Availed On</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($availers as $a)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $a->customer->name }}</td>
                            <td>{{ date('d M, Y', strtotime($a->created_at)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
