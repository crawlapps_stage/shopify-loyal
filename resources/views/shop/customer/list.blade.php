<link href="{{ asset('shop/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
<style>
    span.cc{
        position: absolute;
        right: 15px;
        margin-top: 15px;
    }
</style>
@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b>All Customers</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    {{ TR('Customers') }}
                    <button class="btn btn-sm btn-default waves-effect pull-right m-t--5 font-bold text-uppercase horizon-next">
                        <i class="material-icons" style="color: #333 !important;">keyboard_arrow_right</i>
                    </button>
                    <button class="btn btn-sm btn-default waves-effect pull-right m-t--5 font-bold text-uppercase horizon-prev">
                        <i class="material-icons" style="color: #333 !important;">keyboard_arrow_left</i>
                    </button>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ TR('Name') }}</th>
                            <th>{{ TR('Email') }}</th>
                            <th>{{ TR('DoB') }}</th>
                            <th>{{ TR('Gender') }}</th>
                            <th>{{ TR('Points') }}</th>
                            <th>{{ TR('Spent') }}</th>
                            <th>{{ TR('Balance') }}</th>
                            <th>{{ TR('Barcode') }}</th>
                            <th>{{ TR('Card Barcode') }}</th>
                            <th>{{ TR('Card Barcode 1') }}</th>
                            <th>{{ TR('Joined At') }}</th>
                            <th>{{ TR('Actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($customers as $c)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                                {{ $c->name }}
                                @if($c->is_cancelled)
                                <br />
                                <span class="label label-danger">Cancelled</span>
                                @endif
                            </td>
                            <td>{{ $c->email }}</td>
                            <td>{{ $c->dob }}</td>
                            <td>{{ $c->gender }}</td>
                            <td>{{ $c->total_points }}</td>
                            <td>{{ $c->spending }}</td>
                            <td>{{ $c->remaining_points }}</td>
                            <td>{{ $c->barcode ?: 'NaN' }}</td>
                            <td>{{ $c->plastic_card_barcode ?: 'NaN' }}</td>
                            <td>{{ $c->plastic_card_barcode1 ?: 'NaN' }}</td>
                            <td>{{ date('d M, Y', strtotime($c->created_at)) }}</td>
                            <td>
                                <a class="btn btn-sm btn-info call-url" href="{{ route('shop.customer.earnings', $c->id) }}">{{ TR('Earnings') }}</a>
                                <a class="btn btn-sm btn-info call-url" href="{{ route('shop.customer.spendings', $c->id) }}">{{ TR('Spendings') }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('shop/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('shop/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>