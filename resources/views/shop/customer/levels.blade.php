@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b>Customer Levels</b></br></h3>
</div>
<div class="row clearfix">
    <form method="post" action="{{ route('save.customer.levels') }}">
        @csrf
        <div class="col-md-12">
            <div class="card">
                <div class="header bg-green">
                    <h2>
                        {{ TR('Customer Levels') }}
                    </h2> 
                </div>
                <div class="body table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{ TR('Customer Levels') }}</th>
                                <th>{{ TR('Required Points') }}</th>
                                <th>{{ TR('Extra Reward Points') }} On Every <b>{{ $forEvery }} {{ userShop()->currency }}</b> Spend</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($levels as $lvl)
                            <tr>
                                <td>
                                    <input type="text" class="form-control" placeholder="Title" name="titles[]" value="{{ $lvl->title }}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Required Points" name="required_points[]" value="{{ $lvl->required_points }}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Extra Reward Point(s)" name="extra_points[]" value="{{ !empty($lvl->extraPoint->points) ? $lvl->extraPoint->points : 0 }}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <button class="btn btn-sucess btn-lg waves-effect bg-green">Save Changes</button>
                </div>
            </div>
        </div>
    </form>
</div>
