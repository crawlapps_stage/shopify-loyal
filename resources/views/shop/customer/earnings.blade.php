@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b><i>{{ $customer->name }}'s</i> Earnings</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    <b>{{ $customer->name }}</b> Earnings
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Occasion</th>
                            <th>Points</th>
                            <th>Extra Points</th>
                            <th>T.Points</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($earnings as $e)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $e->occasion }}</td>
                            <td>{{ $e->points }}</td>
                            <td>{{ $e->extra_point_info }}</td>
                            <td>{{ $e->total_points }}</td>
                            <td>
                                @if($e->status)
                                <span class="label label-success">Confirmed</span>
                                @else
                                <span class="label label-danger">Pending</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
