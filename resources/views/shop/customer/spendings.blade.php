@php error_reporting(0); @endphp
<div class="block-header text-center">
    <h3><b><i>{{ $customer->name }}</i>'s</b> Spendings</br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    <b>{{ $customer->name }}</b> Spendings
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table table-hover dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Order</th>
                            <th>{{ TR('Spent') }}</th>
                            <th>Status</th>
                            <th>Discount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($spendings as $s)
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                                <a href="{{ $s->status_url }}" target="_blank" class="btn btn-default">
                                    View Order
                                </a>
                            </td>
                            <td>{{ $s->spent_points }}</td>
                            <td>
                                @if($s->status)
                                <span class="label label-success">Confirmed</span>
                                @else
                                <span class="label label-danger">Pending</span>
                                @endif
                            </td>
                            <td>{{ $s->discount }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
