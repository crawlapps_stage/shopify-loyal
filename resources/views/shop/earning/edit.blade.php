<div class="block-header text-center">
    <h3><b>Update Earning Rule</b></br></h3>
</div>
<div class="row clearfix">
	<form method="post" action="{{ route('update.earning') }}">
		@csrf
		<input type="hidden" name="points_for" value="{{ $rule->id }}">
	    <div class="col-md-6 col-md-offset-3">
	        <div class="card">
	            <div class="body">
	                <h2 class="card-inside-title">{{ $ruleName }} Points</h2>
	                <label>Number of points awarded for this action</label>
                    <div class="m-b-15">
                        <input type="text" class="form-control" placeholder="Number of points" name="points" value="{{ !empty($points) ? $points : $rule->points }}" />
                    </div>
                    @if($rule->earning_action_id == 5)
                    <label>Required Spending</label>
                    <div class="m-b-15">
                        <div class="input-group">
                            <input type="text" class="form-control" name="required_shopping" value="{{ $forEvery}}">
                            <span class="input-group-addon with-border">{{ userShop()->currency }}</span>
                        </div>
                    </div>
                    @endif
                    <button class="btn btn-sucess btn-lg waves-effect bg-green">Save</button>
	            </div>
	        </div>
	    </div>
    </form>
</div>