<div class="block-header text-center">
    <h3><b>Earning Rules</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header bg-green">
                <h2>
                    Earning Rules
                </h2> 
            </div>
            <div class="body table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Earning Type</th>
                            <th>Points Value</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($earningRules as $er)
                    	@if($er->earningAction->name == 'Visit') @continue @endif
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>
                            	<a href="{{ route('edit.earning', $er->id) }}" class="call-url">
                            		{{ $er->earningAction->name }}
                            	</a>
                            </td>
                            <td>
                            	@if($er->earning_action_id == 5)
                                @php list($points, $forEvery) = explode(',', $er->points) @endphp
                            	{{ $points }} Point(s) for every {{ $forEvery }}{{ userShop()->currency }} spending
                            	@else
                            	{{ $er->points }} Points
                            	@endif
                            </td>
                            <td>
                            	@if($er->status)
                            	<span class="label bg-green">Active</span>
                            	@else
                            	<span class="label bg-red">Disabled</span>
                            	@endif
                            </td>
                            <td>
                                @if($er->status)
                                <a href="{{ route('update.earning.status', [$er->id, 0]) }}" class="btn btn-warning waves-effect btn-sm action-url">
                                    <i class="material-icons">lock</i>
                                </a>
                                @else
                                <a href="{{ route('update.earning.status', [$er->id, 1]) }}" class="btn btn-primary waves-effect btn-sm action-url">
                                    <i class="material-icons">lock_open</i>
                                </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
