<div class="block-header text-center">
    <h3><b>Dashboard</b></br></h3>
</div>
<div class="row clearfix">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-4 hover-expand-effect">
            <div class="icon">
                <i class="material-icons col-green">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text col-green"><b>{{ TR('Campaigns') }}</b></div>
                <div class="number count-to" data-from="0" data-to="{{ count($campaigns) }}" data-speed="15" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-4 hover-expand-effect">
            <div class="icon">
                <i class="material-icons col-green">person</i>
            </div>
            <div class="content">
                <div class="text col-green"><b>{{ TR('Customers') }}</b></div>
                <div class="number count-to" data-from="0" data-to="{{ count($customers) }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="info-box-4 hover-expand-effect">
            <div class="icon">
                <i class="material-icons col-green">playlist_add_check</i>
            </div>
            <div class="content">
                <div class="text col-green"><b>{{ TR('Availed Offers') }}</b></div>
                <div class="number count-to" data-from="0" data-to="{{ count($availedOffers) }}" data-speed="1000" data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card" style="min-height: 380px">
            <div class="header bg-green">
                <h2>{{ TR('Recent Customers') }}</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ TR('Name') }}</th>
                                <th>{{ TR('Balance') }}</th>
                                <th>{{ TR('Actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($customers->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-7 days'))) as $c)
                        	<?php if($loop->iteration == 10) break; ?>
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $c->name }}</td>
                                <td>{{ $c->remaining_points }}</td>
                                <td>
                                    <a class="btn btn-sm btn-info" href="{{ route('shop.customer.earnings', $c->id) }}">{{ TR('Earnings') }}</a>
                                    <a class="btn btn-sm btn-info" href="{{ route('shop.customer.spendings', $c->id) }}">{{ TR('Spendings') }}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>