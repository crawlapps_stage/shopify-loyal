<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                @php $route = Route::currentRouteName() @endphp
                <li class="nav-li {{ $route == 'shop.dashboard' ? 'active' : '' }}">
                    <a href="{{ route('shop.dashboard') }}" class="call-url">
                        <i class="material-icons">home</i>
                        <span>{{ __('Home') }}</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">people</i>
                        <span>{{ __('Customers') }}</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="nav-li">
                            <a href="{{ route('customers') }}" class="call-url">
                                <span>{{ __('All Customers') }}</span>
                            </a>
                        </li>
                        <li class="nav-li">
                            <a href="{{ route('customer.levels') }}" class="call-url">
                                <span>{{ __('Customer Levels') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @if(userShop()->plan > 1)
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">view_list</i>
                        <span>{{ __('Campaigns') }}</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="nav-li ">
                            <a href="{{ route('campaigns') }}" class="call-url">
                                <span>{{ __('Campaigns') }}</span>
                            </a>
                        </li>
                        <li class="nav-li ">
                            <a href="{{ route('offers') }}" class="call-url">
                                <span>{{ __('Offers') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                @if(userShop()->plan == 3)
                <li>
                    <a href="javascript:;" data-toggle="modal" data-target="#upload-csv">
                        <i class="material-icons">view_list</i>
                        <span>{{ __('Upload CSV') }}</span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">settings</i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="nav-li {{ $route == 'earning.rules' ? 'active' : '' }}">
                            <a href="{{ route('earning.rules') }}" class="call-url">
                                <span>{{ __('Reward Program') }}</span>
                            </a>
                        </li>
                        <li class="nav-li {{ $route == 'display' ? 'active' : '' }}">
                            <a href="{{ route('display') }}" class="call-url">
                                <span>Display</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span>{{ __('Discounts') }}</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="nav-li">
                                    <a href="{{ route('events') }}" class="call-url">
                                        <span>{{ __('Events') }}</span>
                                    </a>
                                </li>
                                <li class="nav-li">
                                    <a href="{{ route('discounts', 'event') }}" class="call-url">
                                        <span>{{ __('Per Event') }}</span>
                                    </a>
                                </li>
                                <li class="nav-li">
                                    <a href="{{ route('discounts', 'level') }}" class="call-url">
                                        <span>{{ __('Customer Levels') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-li {{ $route == 'choose.plan' ? 'active' : '' }}" class="call-url">
                    <a href="{{ route('choose.plan') }}">
                        <i class="material-icons">settings</i>
                        <span>{{ __('Subscription Plans') }}</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright text-center">
                &copy; {{ date('Y') }} <a href="javascript:void(0);">Shoployal App</a> |
                <a class="float-right" target="_blank" href="https://shoployal.ie/home/shoployal-privacy-policy"> Privacy & Policy</a>
            </div>

        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
@if(userShop()->plan == 3)
<div id="upload-csv" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload CSV</h4>
      </div>
      <div class="modal-body">
        <form method="post">
            @csrf
            <div class="m-b-15">
                <input type="file" name="csv" class="form-control" style="padding: 0px">
            </div>
            <button class="btn btn-success">Upload CSV</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif
