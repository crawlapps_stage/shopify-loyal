<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    public function campaignItems()
    {
    	return $this->hasMany('App\CampaignItem');
    }
    public function extraPoint()
    {
    	return $this->hasOne('App\JumpPoint', 'based_on_id')->where('based_on', 1);
    }
    public function discount()
    {
    	return $this->hasOne('App\DiscountRule', 'based_on_id')->where('based_on', 1);
    }
}
