<?php

namespace App\Http\Middleware;

use Closure;
use Oseintow\Shopify\Facades\Shopify;

class Installation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Shopify::verifyRequest($request->getQueryString()))
            return $next($request);
        
        dd("Bad Request!");
    }
}
