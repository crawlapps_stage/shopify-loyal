<?php

namespace App\Http\Middleware;

use Closure;
use App\Shop;
use App\Customer;
use Oseintow\Shopify\Facades\Shopify;
use Log;

class CustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->customer) || empty($request->shop))
        {
            return ("AUTHORIZATION FAILED!");
            exit();
        }
        else
        {
            $customer = Customer::whereCustomerId($request->customer)->first();
            $shop = Shop::whereUrl($request->shop)->first();
            if(empty($shop))
            {
                return ("AUTHORIZATION FAILED!");
                exit();
            }
            if($shop->status)
            {
                if(empty($customer))
                {
                    $resource = Shopify::setShopUrl($request->shop)
                    ->setAccessToken($shop->access_token);
                    try {
                        $cus = $resource->get("admin/customers/".$request->customer.".json")->toArray();
                    } catch (\Exception $e) {
                        return ("SOMETHING WENT WRONG, TRY AGAIN.");
                        exit();
                    }
                    $forJoining = $request;
                    return response(view('layouts.customer_app', compact('forJoining')));
                }
                return $next($request);
            }
        }
    }
}
