<?php

namespace App\Http\Middleware;

use Closure;
use Oseintow\Shopify\Facades\Shopify;

class WebHooks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->getContent();
        $hmacHeader = $request->server('HTTP_X_SHOPIFY_HMAC_SHA256');

        if (Shopify::verifyWebHook($data, $hmacHeader))
            return $next($request);

        dd("verification failed");
    }
}
