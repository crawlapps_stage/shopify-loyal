<?php

namespace App\Http\Middleware;

use Closure;

class Premium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(in_array(userShop()->plan, [2,3]))
            return $next($request);
        else
            return redirect()->route('choose.plan')
            ->with('info', 'Please choose a premium plan to continue action');
    }
}
