<?php

namespace App\Http\Middleware;

use Closure;
use App\Shop;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($_SERVER['HTTP_ORIGIN']))
            dd("DIRECT ACCESS IS NOT ALLOWED!");
        // \App::setLocale('hu');
        $http_origin = $_SERVER['HTTP_ORIGIN'];
        $check_origin = str_replace("https://", "", $http_origin);
        if(Shop::whereUrl($check_origin)->first())
        {
            return $next($request)
            ->header('Access-Control-Allow-Origin', $http_origin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');   
        }
    }
}