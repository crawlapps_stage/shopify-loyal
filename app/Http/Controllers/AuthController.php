<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use App\Shop;

class AuthController extends Controller
{
    public function shopLogin($url = '')
    {
        if($url != '')
            $shop = Shop::whereUrl($url)->first();
    	return view('shop.login', 'shop');
    }
    public function authenticate(Request $req)
    {
    	$req->validate([
    		'shop_url' => 'required|exists:shops,url',
    		'password' => 'required'
    	]);

    	$shop = Shop::whereUrl($req->shop_url)->first();
    	if(Hash::check($req->password, $shop->password))
    	{
    		Auth::login($shop);
    		return redirect()->route('shop.dashboard');
    	}
    	else
    	{
    		return back()->with('error', 'Incorrect shop url OR password');
    	}
    	return view('shop.login');
    }
}