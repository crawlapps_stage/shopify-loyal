<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;

class CheckShopifyWebhook extends Controller
{
    public function get(Request $request)
    {
        $shop = shopResource();

        /*$routesPrefix =  'https://9699ac77.ngrok.io'.'/api/webhooks/';
        $response = $shop->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "shop/update",
                    "address"=> $routesPrefix.'shop-update',
                    "format"=> "json"
                ]
            ]
        );

       dd($response);*/

        $data = $shop->get('/admin/webhooks.json');
        dd($data);
    }

    public function destroy(Request $request)
    {
        $shop = shopResource();
        $data = $shop->delete('/admin/webhooks/'.$request->id.'.json');
        dd($data);
    }
}
