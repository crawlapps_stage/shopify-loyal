<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\JumpPoint;
use App\EarningRule;

class EventController extends Controller
{
	public function index()
	{
		$events = Event::whereShopId(userShopId())->get();
		return view('shop.event.list', compact('events'));
	}
	public function add()
	{
		$earningRules = EarningRule::whereShopId(userShopId())->with('earningAction')->get();
		return view('shop.event.add', compact('earningRules'));
	}
	public function save(Request $req, $id = null)
	{
		$req->validate([
			'name' => 'required|max:191',
			'starts_at' => 'required'
		]);
		if(is_null($id))
		{
			$event = new Event();
		}
		else
		{
			$event = Event::where([
				'id' => $id, 'shop_id' => userShopId()
			])->first();
			if(empty($event))
    			return notFound('Event', 'add.event');
		}
		$event->shop_id = userShopId();
		$event->name = $req->name;
		$event->starts_at = $req->starts_at;
		$event->ends_at = $req->ends_at;
		$event->save();

		$count = 0;
		$earningRules = EarningRule::whereShopId(userShopId())->get();
		foreach ($earningRules as $key => $er)
		{
			if($req->points[$key] == '' || $req->points[$key] == 0)
				continue;
			insertJumpPointRule($req->points[$key], 2, $event->id, $er->earning_action_id);

		}

		return ['success', 'Event saved'];
	}
	public function edit($id)
	{
		$event = Event::where([
			'id' => $id, 'shop_id' => userShopId()
		])->with('extraPoints')->first();

		if(empty($event))
			return notFound('Event', 'events');

		$earningRules = EarningRule::whereShopId(userShopId())->with('earningAction')->get();
		return view('shop.event.edit', compact('event', 'earningRules'));
	}
	public function delete($id)
	{
		$event = Event::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($event))
			return notFound('Event', 'events');

		$event->delete();
		return ['success', 'Event deleted'];
	}
}
