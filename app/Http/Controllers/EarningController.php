<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EarningRule;

class EarningController extends Controller
{
    public function index()
    {
    	$earningRules = EarningRule::whereShopId(userShopId())->with('earningAction')->get();
    	return view('shop.earning.list', compact('earningRules'));
    }
    public function edit($id)
    {
        error_reporting(0);
    	$rule = EarningRule::where([
            'id' => $id, 'shop_id' => userShopId()
        ])->first();

    	if(empty($rule))
    		return notFound('Earning rule', 'earning.rules');

    	$ruleName = $rule->earningAction->name;
        if($rule->earning_action_id == 5)
            list($points, $forEvery) = explode(',', $rule->points);
    	return view('shop.earning.edit', compact('rule', 'ruleName', 'points', 'forEvery'));
    }
    public function update(Request $req)
    {
    	$rule = EarningRule::where([
            'id' => $req->points_for, 'shop_id' => userShopId()
        ])->first();

    	if(empty($rule))
    		return notFound('Earning rule', 'earning.rules');

    	$req->validate([
    		'points' => 'required|integer',
    		'required_shopping' => $rule->earning_action_id == 5 ? 'required|numeric' : ''
    	]);

    	$rule->points = $req->points;
        if($rule->earning_action_id == 5)
            $rule->points .= ','.$req->required_shopping;
    	$rule->save();

    	return ['success', 'Earning rule updated'];
    }
    public function updateStatus($id, $status)
    {
        $rule = EarningRule::where([
            'id' => $id, 'shop_id' => userShopId()
        ])->first();

    	if(empty($rule))
    		return notFound('Earning rule', 'earning.rules');
    		
    	$rule->status = $status;
    	$rule->save();
    	
    	return ['success', 'Earning rule status has been changed'];
    }
}