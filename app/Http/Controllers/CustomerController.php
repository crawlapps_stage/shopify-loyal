<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;
use GuzzleHttp\Client;

use App\Shop;
use App\Event;
use App\Customer;
use App\EarningRule;
use App\EarningDetail;
use App\SpendingDetail;
use App\DiscountRule;
use App\CustomerLevelPoint;
use App\Campaign;
use App\EarningDetailProduct;

class CustomerController extends Controller
{
    public function index(Request $req)
    {
        $shop = Shop::whereUrl($req->shop)->first();
        if(empty($shop))
            dd('PAGE NOT FOUND!');
        if($shop->status)
            return view('layouts.customer_app');
    }
    public function home()
    {
        return view('customer.index');
    }
    public function join(Request $request, $appJoined = null)
    {
        error_reporting(0);
        $customer = Customer::whereCustomerId($request->customer)->first();
        $shop = Shop::whereUrl($request->shop)->first();
        if(empty($shop))
        {
            dd("AUTHORIZATION FAILED!");
        }
        if(empty($customer))
        {
            if(is_null($appJoined))
            {
                try {
                    $cus = shopResource($shop)->get("admin/customers/".$request->customer.".json")->toArray();
                } catch (\Exception $e) {
                    dd("SOMETHING WENT WRONG, TRY AGAIN.");
                }
            }
            else
                $cus = $appJoined;
            $c = new Customer();
            $c->shop_id = $shop->id;
            $c->customer_id = (string)$cus['id'];
            $c->name = $cus['first_name'] . ' ' . $cus['last_name'];
            $c->email = $cus['email'];
            $c->dob = $cus['dob'];
            $c->gender = $cus['gender'];
            $c->save();
            saveEarning($c, $shop->id, 2);
            updateLevel($c, false);
            
            if($shop->plan == 3)
            {
                if(is_null($appJoined))
                {
                    $result = $this->postDataToSL([
                        'method' => 'GetUserBarcode',
                        'params' => array('shopifyUserId' => $c->customer_id)
                    ]);
                    $c->barcode = $result['result'];
                }
                else
                    $c->barcode = $appJoined['barcode'];
            }
            $c->save();
            
            // \App::setLocale('hu');
            // sendMail([
            //     'subject' => __('Thank you for joining'),
            //     'to' => $c->email,
            //     'customer' => $c,
            //     'blade' => 'joining_new'
            // ]);
        }
        if($request->barcode && $shop->plan == 3)
        {
            $isLinked = $this->linkBarcode($request, true);
            if($isLinked != 'OK')
                return $isLinked;
        }
        return $this->dashboard($request, 'true');
    }
    public function dashboard(Request $req, $joined = '')
    {
        $customer = authUser();
        if($req->required == 'layout')
            return view('layouts.customer_app');
        if($customer->is_cancelled)
            return view('customer.cancel', ['cancelled' => true]);
            
        return view('customer.dashboard', compact('customer', 'joined'));
    }
    public function earn()
    {
        return $this->loyaltyLevels();
        
        $earningRules = EarningRule::whereStatus(1)->whereShopId(userShopId())->with('earningAction')->get();
        return view('customer.earn', compact('earningRules'));
    }
    public function earnings()
    {
        $earnings = EarningDetail::where([
            'shop_id' => userShopId(), 'customer_id' => userId()
        ])->get();
        return view('customer.earnings', compact('earnings'));
    }
    public function spend()
    {
        $campaigns = Campaign::where([
            'shop_id' => userShopId(), 'is_active' => 1
        ])
        ->where('required_points' ,'<=', authUser()->remaining_points)
        ->where('starts_at' ,'<=', date('Y-m-d H:i:s'))
        ->where('ends_at' ,'>=', date('Y-m-d H:i:s'))
        ->with('discount', 'campaignItems')->get();

        $events = Event::where([
            'shop_id' => userShopId(), 'status' => 1
        ])
        ->where('starts_at' ,'<=', date('Y-m-d H:i:s'))
        ->where('ends_at' ,'>=', date('Y-m-d H:i:s'))
        ->with('discounts')->get();

        $discounts = DiscountRule::where([
            'shop_id' => userShopId(), 'status' => 1, 'based_on' => 3,
            'based_on_id' => authUser()->level_id
        ])->get();
        return view('customer.spend', compact('campaigns', 'events', 'discounts'));
    }
    public function spendings()
    {
    //  $spendings = SpendingDetail::where([
    //      'shop_id' => userShopId(), 'customer_id' => userId()
    //  ])->get();
        $pendings = EarningDetail::where([
            'shop_id' => userShopId(), 'customer_id' => userId(),
            'status' => 0
        ])->where('order_id', '!=', null)->get();
        $completed = EarningDetail::where([
            'shop_id' => userShopId(), 'customer_id' => userId(),
            'status' => 1
        ])->where('order_id', '!=', null)->get();
        return view('customer.spendings', compact('pendings', 'completed'));
    }
    public function spendingDetails($id)
    {
        $details = EarningDetailProduct::where([
            'earning_detail_id' => $id
        ])->get();
        $earning = EarningDetail::find($id);
        return view('customer.spending_details', compact('details', 'earning'));
    }

    public function loyaltyLevels()
    {
        $levels = CustomerLevelPoint::where('shop_id', userShopId())->get();
        return view('customer.earn', compact('levels'));
    }

    public function onDelete(Request $req)
    {
        $data = json_decode($req->getContent(), true);
        Customer::whereCustomerId($data['id'])->delete();
    }
    
    public function addBarcode()
    {
        return view('customer.add_barcode');
    }
    public function linkBarcode(Request $req, $whileJoining = false)
    {
        $customer = authUser();
        if(empty($customer->plastic_card_barcode))
        {
            $barcode = $req->barcode;
            $offlineCheck = Customer::where('plastic_card_barcode', $barcode)
                               ->where('customer_id','like','UNIQUE_%')->first();
            if($offlineCheck)
            {
                EarningDetail::whereCustomerId($offlineCheck->id)->update([
                    'customer_id' => $customer->id
                ]);
                SpendingDetail::whereCustomerId($offlineCheck->id)->update([
                    'customer_id' => $customer->id
                ]);
                $customer->total_points += $offlineCheck->total_points;
                $customer->remaining_points += $offlineCheck->remaining_points;
                $customer->spending += $offlineCheck->spending;
                $customer->save();
                $offlineCheck->delete();
            }
            $check = Customer::where('barcode', $barcode)
                    ->orWhere('plastic_card_barcode', $barcode)
                    ->orWhere('plastic_card_barcode1', $barcode)->first();
            if($check)
                $error = 'Invalid Barcode';
            else if(empty($barcode))
                $error = 'Barcode is required';
            else if(strlen($barcode) != 13)
                $error = 'Barcode length must be 13';
            else if(!ctype_digit($barcode))
                $error = 'Barcode must contain digits only';
            else
            {
                $result = $this->postDataToSL([
                    "params" => array(
                        'barcode' => $barcode ?: 0
                    ),
                    "method"=> "IsValidBarcode",
                ]);
                if($result['result'] == '')
                    $error = 'Invalid Barcode';
            }
            if(empty($error))
            {
                $customer->plastic_card_barcode = $barcode;
                $customer->save();
                $success = "Barcode has been successfully linked with your account";
                if($whileJoining)
                    return 'OK';
            }
        }
        
        if(is_null($customer->plastic_card_barcode1) && !is_null($customer->plastic_card_barcode))
        {
            $barcode1 = $req->barcode1;
            if(!empty($barcode1))
            {
                $check = Customer::where('barcode', $barcode1)
                    ->orWhere('plastic_card_barcode', $barcode1)
                    ->orWhere('plastic_card_barcode1', $barcode1)->first();
                if($check)
                    $error1 = 'Invalid Barcode';
                else if(strlen($barcode1) != 13)
                    $error1 = 'Barcode length must be 13';
                else if(!ctype_digit($barcode1))
                    $error1 = 'Barcode must contain digits only';
                else
                {
                    $result = $this->postDataToSL([
                        "params" => array(
                            'barcode' => $barcode1 ?: 0
                        ),
                        "method"=> "IsValidBarcode",
                    ]);
                    if($result['result'] == '')
                        $error1 = 'Invalid Barcode';
                }
                if(empty($error1))
                {
                    $customer->plastic_card_barcode1 = $barcode1;
                    $customer->save();
                    $success1 = "Barcode has been successfully linked with your account";
                }
            }
        }
        return view('customer.add_barcode', compact('error', 'success', 'error1', 'success1'));
    }

    public function saveAppUser(Request $req)
    {
        $shop = Shop::whereUrl($req->shop)->first();
        if(empty($shop) || empty($req->customer) || empty($req->unique_id))
            dd("AUTHORIZATION FAILED!");

        try {
            $cus = shopResource($shop)->get("admin/customers/".$req->customer.".json")->toArray();
        } catch (\Exception $e) {
            dd("SOMETHING WENT WRONG, TRY AGAIN.");
        }

        $result = $this->postDataToSL([
            "params" => array(
                'shopifyId' => (string)$cus['id'], 
                'firstName' => $cus['first_name'],
                'lastName' => $cus['last_name'],
                'email' => $cus['email'],
                'uniqueId' => $req->unique_id
            ),
            "method"=> "AddUserAcc",
        ]);
        if(!empty($result['result']['barcode']))
        {
            $cus['barcode'] = $result['result']['barcode'];
            $isExist = Customer::whereCustomerId((string)$cus['id'])->first();
            if(empty($isExist))
            {
                return $this->join($req, $cus);
            }
            else
            {
                $isExist->barcode = $result['result']['barcode'];
                $isExist->save();
            }
        }
        return '';
    }

    public function postDataToSL($fields)
    {
        $ch = curl_init("https://jateksziget.shoployal.ie/api/users");
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode(array_merge($fields, [
            "id" => 443,
            "jsonrpc" => "2.0"
        ])));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $result), true );
    }

    public function getLevels(Request $req)
    {
        $data = json_decode($req->getContent(), true);
        $customer = Customer::whereBarcode($data['params']['barcode'])->first();
        if($customer)
        {
            $levels = CustomerLevelPoint::where('shop_id', $customer->shop_id)->get();
            return json_encode([
                'currentLevelId' => $customer->level_id,
                'levels' => $levels
            ]);
        }
    }
    
    public function cancellation()
    {
        return view('customer.cancel');
    }
    public function cancel()
    {
        $cus = authUser();
        $cus->is_cancelled = 1;
        $cus->save();
        
        return view('customer.cancel', ['cancelled' => true]);
    }
    
    public function testing(){
        foreach (Customer::all() as $key => $value) {
            $value->name = utf8_decode($value->name);
            $value->save();
        }
    }
}
