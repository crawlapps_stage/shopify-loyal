<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\DiscountSetup;

use App\Campaign;
use App\CampaignItem;
use App\CustomerLevelPoint;
use App\EarningRule;
use App\JumpPoint;
use App\DiscountRule;

class CampaignController extends Controller
{
	public function index()
	{
		$campaigns = Campaign::whereShopId(userShopId())->get();
		return view('shop.campaign.list', compact('campaigns'));
	}
    public function add()
    {
    	$levels = CustomerLevelPoint::whereShopId(userShopId())->get();
    	return view('shop.campaign.add', compact('levels'));
    }
    public function save(Request $req, $id = null)
    {
    	$req->validate([
    		'name' => 'required|max:191',
    		'starts_at' => 'required',
    		'points' => 'required|integer|min:1',
	    	'discount_type' => 'required|in:1,2',
	    	'discount' => 'required|numeric'
    	]);
    	if($req->target_level)
    		$req->validate(['target_level' => 'exists:customer_levels,id']);
    	if($req->target_age_from)
    		$req->validate(['target_age_from' => 'integer']);
    	if($req->target_age_to)
    		$req->validate(['target_age_to' => 'integer']);
    	if($req->if_birthday)
    		$req->validate(['if_birthday' => 'in:1,0']);
    	if($req->target_gender)
    		$req->validate(['target_gender' => 'in:male,female']);
    	if($req->extra_points)
    		$req->validate(['extra_points' => 'integer']);

    	if(is_null($id))
		{
			$campaign = new Campaign();
		}
		else
		{
			$campaign = Campaign::where([
				'id' => $id, 'shop_id' => userShopId()
			])->first();
			if(empty($campaign))
    			return notFound('Campaign', 'add.campaign');
		}

		$campaign->shop_id = userShopId();
		$campaign->name = $req->name;
		$campaign->starts_at = $req->starts_at;
		$campaign->ends_at = $req->ends_at;
		$campaign->target_level = $req->target_level;
		$campaign->target_gender = $req->target_gender;
		$campaign->target_age_from = $req->target_age_from ?: 3;
		$campaign->target_age_to = $req->target_age_to ?: 80;
		$campaign->if_birthday = $req->if_birthday;
		$campaign->required_points = $req->points;
        $campaign->discount = $req->discount;
        $campaign->discount_type = $req->discount_type;
        $campaign->discount_code = generateCoupon();
		$campaign->save();
		return ['success', 'Campaign saved'];
    }
    public function edit($id)
    {
    	$campaign = Campaign::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($campaign))
			return notFound('Campaign', 'campaigns');

		$levels = CustomerLevelPoint::whereShopId(userShopId())->get();
		return view('shop.campaign.edit', compact('campaign', 'levels'));
    }
    public function delete($id)
    {
    	$campaign = Campaign::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($campaign))
			return notFound('Campaign', 'campaigns');

		$campaign->delete();
		return ['success', 'Campaign deleted'];
    }
    public function updateStatus($id, $status)
    {
    	$campaign = Campaign::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($campaign))
			return notFound('Campaign', 'campaigns');
		$campaign->is_active = $status;
		$campaign->save();

		return ['success', 'Campaign ' . ($status == 1 ? 'activated' : 'de-actived')];
    }

    public function availedItems($id)
    {
    	
    }
    public function itemAvailers($id)
    {

    }
}
