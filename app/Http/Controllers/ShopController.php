<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\DiscountSetup;

use App\CustomerLevel;
use App\CustomerLevelPoint;
use App\DiscountRule;
use App\JumpPoint;
use App\EarningDetail;
use App\SpendingDetail;
use App\Customer;
use App\Campaign;
use App\AvailedOffer;
use App\Shop;

use Laravel\Cashier\Exceptions\IncompletePayment;

class ShopController extends Controller
{
    public function index()
    {
        $campaigns = Campaign::whereShopId(userShopId())->get();
        $customers = Customer::whereShopId(userShopId())->get();
        $availedOffers = AvailedOffer::whereShopId(userShopId())->with('campaign', 'campaignItem', 'customer')->get();
    	return view('shop.index', compact('campaigns', 'customers', 'availedOffers'));
    }
    public function enable()
    {
        $shop = Shop::find(userShopId());
        $shop->status = 1;
        $shop->save();

        return ['success', 'App is Enabled'];
    }
    public function disable()
    {
        $shop = Shop::find(userShopId());
        $shop->status = 0;
        $shop->save();
        return ['success', 'App is Disabled'];
    }

    /** CUSTOMERS **/
    public function customers()
    {
    	$customers = Customer::whereShopId(userShopId())->get();
    	return view('shop.customer.list', compact('customers'));
    }
    public function customerEarnings($id)
    {
    	$earnings = EarningDetail::where([
    		'shop_id' => userShopId(),
    		'customer_id' => $id
    	])->get();
    	$customer = Customer::where([
    		'shop_id' => userShopId(),
    		'id' => $id
    	])->first();
    	if(empty($customer))
    		return notFound('Customer', 'customers');
    	return view('shop.customer.earnings', compact('earnings', 'customer'));
    }
    public function customerSpendings($id)
    {
    	$spendings = SpendingDetail::where([
    		'shop_id' => userShopId(),
    		'customer_id' => $id
    	])->get();
    	$customer = Customer::where([
    		'shop_id' => userShopId(),
    		'id' => $id
    	])->first();
    	if(empty($customer))
    		return notFound('Customer', 'customers');
    	return view('shop.customer.spendings', compact('spendings', 'customer'));
    }

    /** CUSTOMER LEVELS **/
    public function customerLevels()
    {
    	$levels = CustomerLevelPoint::where('shop_id', userShopId())
    			->with('extraPoint', 'generalDiscount')->get();
    	list($point, $forEvery) = spendingRule();
    	return view('shop.customer.levels', compact('levels', 'forEvery'));
    }
    public function updateCustomerLevels(Request $req)
    {
    	$req->validate([
    		'required_points.*' => 'required|numeric',
    		'extra_points.*' => 'numeric',
    		'discount.*' => 'numeric',
    		'discount_type.*' => 'in:1,2,3',
    		'titles.*' => 'required|max:191'
    	]);
    	$levels = CustomerLevelPoint::where('shop_id', userShopId())->get();
    	$count = 0;
    	foreach ($levels as $lvl)
    	{
    	    $lvl->title = $req->titles[$count];
    		$lvl->required_points = $req->required_points[$count];
    		$lvl->save();

    		insertJumpPointRule($req->extra_points[$count], 3, $lvl->id, 5);

    		if($req->discount[$count] == '' || $req->discount[$count] == 0)
    		{
    			if($lvl->generalDiscount)
    			{
	    			DiscountRule::where([
		    			'shop_id' => userShopId(), 'based_on' => 3, 'based_on_id' => $lvl->id,
		    			'points' => 0
		    		])->delete();
	    		}
    		}
    		else
    		{
    			$discountSetupReq = (object)[
					'per' => 'general', 'based_on_id' => $lvl->id,
					'points' => 0, 'discount' => $req->discount[$count] ?: 0,
					'discount_type' => $req->discount_type[$count]
				];
				try
		    	{
	    			$discountRule = DiscountRule::where([
		    			'shop_id' => userShopId(), 'based_on' => 3, 'based_on_id' => $lvl->id,
		    			'points' => 0
		    		])->first();
		    		if($discountRule)
		    			$discountRuleId = $discountRule->id;
		    		else
		    			$discountRuleId = null;
		    		$res = DiscountSetup::setup($discountSetupReq, $discountRuleId);
		    	}
		    	catch (\Exception $e)
		    	{
		    		return somethingWrong('customer.levels');
		    	}
    		}

    		$count++;
    	}
    	return ['success', 'Customer level setting updated'];
    }

    public function choosePlan()
    {

        //dd(authUser());
        if(authUser()){
            return view(
                'choose_plan',
                [
                    'intent' => authUser()->createSetupIntent(),
                    'shop' => userShop()
                ]
            );
        }
        abort('Your store is not authorized');
    }
    public function subscribe(Request $request)
    {
        $paymentMethod = $request->payment_method;
        $plan = $request->plan;

        if($plan == 'online-loyalty')
            $intPlan = 2;
        else if($plan == 'online-and-offline')
            $intPlan = 3;
        else if($plan == 'enterprise')
            $intPlan = 4;

        $shop = userShop();

        if(!in_array($plan, ['online-loyalty', 'online-and-offline', 'enterprise']))
            return back()->with('error', 'Something went wrong, please try again.');

        //if($shop->plan < 2)
            authUser()->createAsStripeCustomer();

        if (authUser()->subscribedToPlan($plan, 'main') && !authUser()->subscription('main')->onGracePeriod()) {
            return back()->with('info', 'Already subscribed to ' . revertSlug($plan));
        }

        try {
            // if (authUser()->hasPaymentMethod()) {
            //     $paymentMethod = authUser()->updateDefaultPaymentMethod($paymentMethod);
            //     authUser()->subscription('main')->swap($plan);
            // }
            // else{
                $paymentMethod = authUser()->addPaymentMethod($paymentMethod);

                authUser()->newSubscription('main', $plan)
                ->create($request->payment_method);
            //}

            $shop->plan = $intPlan;
            if(is_null($shop->company_id) && $request->company_id)
                $shop->company_id = $request->company_id;
            $shop->save();
        }
        catch (IncompletePayment $exception) {
            return redirect()->route(
                'choose.plan',
                [$exception->payment->id, 'redirect' => route('shop.dashboard')]
            );
        }

        if($request->company_id)
        {
            $company = DB2()->select('select * from offer where companyId='.$request->company_id);
            if($company)
            {
                $shop = userShop();
                $shop->company_id = $request->company_id;
                $shop->save();
            }
        }
        return $this->updatePlan($intPlan);
    }

    public function display()
    {
        return view('shop.display', ['shop' => userShop()]);
    }

    public function updateDisplay(Request $req)
    {
        $req->validate([
            'button_text' => 'required|max:191|string',
            'header_text' => 'required|max:191|string',
            'background_color' => 'required|max:10|string'
        ]);
        $shop = userShop();
        $shop->button_text = $req->button_text;
        $shop->header_text = $req->header_text;
        $shop->background_color = $req->background_color;
        $shop->save();

        return ['success', 'Display setting updated'];
    }
}
