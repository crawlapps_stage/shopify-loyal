<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\DiscountSetup;

use App\DiscountRule;
use App\Event;
use App\CustomerLevelPoint;

class Discounts extends Controller
{
	use DiscountSetup;

    public function index($per)
    {
    	if(!in_array($per, ['event', 'level']))
    		return somethingWrong('discounts', $per);
    	if($per == 'campaign')
    		$based_on = 1;
    	else if($per == 'event')
    		$based_on = 2;
    	else
    		$based_on = 3;
    	$discounts = DiscountRule::whereShopId(userShopId())
    	->where('based_on', $based_on)->where('points', '!=', '0')->get();
    	return view('shop.discount.list', compact('discounts', 'per'));
    }
    public function add($per)
    {
    	if(!in_array($per, ['event', 'level']))
    		return somethingWrong('shop.dashboard');

        $events =[];$levels=[];
    	if($per == 'event')
    		$events = Event::whereShopId(userShopId())->get();
    	else
    		$levels = CustomerLevelPoint::whereShopId(userShopId())->get();

    	return view('shop.discount.add', compact('per', 'events', 'levels'));
    }
    public function save(Request $req, $id = null)
    {
    	$req->validate([
    	    'coupon_code' => 'required|max:191',
    		'points' => 'required|integer|min:1',
    		'discount_type' => 'required|in:1,2,3'
    	]);
        if(userShop()->discountRules()->where('code', $req->coupon_code)->first())
            $req->validate(['coupon_code' => 'unique:discount_rules,code']);
    	if($req->discount_type != '3')
    		$req->validate(['discount' => 'required|numeric']);
    	try
    	{
    		$discountSetupResponse = DiscountSetup::setup($req, $id);
	    	if($discountSetupResponse != 'OK')
	    		return somethingWrong((is_null($id) ? 'add.discount' : 'discounts'), $req->per);
    	}
    	catch (\Exception $e)
    	{
    		return somethingWrong((is_null($id) ? 'add.discount' : 'discounts'), $req->per);
    	}

	    return ['success', 'Discount rule saved'];
    }
    public function edit($per, $id)
    {
    	if(!in_array($per, ['event', 'level']))
    		return somethingWrong('shop.dashboard');

    	$discount = DiscountRule::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($discount))
			return notFound('Discount rule', 'discounts', $per);

		if($per == 'event')
    		$events = Event::whereShopId(userShopId())->get();
    	else
    		$levels = CustomerLevelPoint::whereShopId(userShopId())->get();

		return view('shop.discount.edit', compact('discount', 'per', 'campaigns', 'events', 'levels'));
    }
    public function delete($per, $id)
    {
        $discount = DiscountRule::where([
			'id' => $id, 'shop_id' => userShopId()
		])->first();
		if(empty($discount))
			return notFound('Discount rule', 'discounts', $per);

		try{
		    shopResource()->delete(
		        "admin/price_rules/".$discount->price_rule_id."/discount_codes/".$discount->discount_code_id.".json"
		    );
		    shopResource()->delete(
		        "admin/price_rules/".$discount->price_rule_id.".json"
		    );
		}
		catch (\Exception $e){
    		return somethingWrong('discounts', $per);
    	}
    	$discount->delete();

    	return ['success', 'Discount rule deleted'];
    }
}
