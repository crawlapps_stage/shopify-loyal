<?php

namespace App\Http\Controllers;

use App\Charge;
use App\Services\ShopifyPaymentGateway;
use Illuminate\Http\Request;

class ChargeController extends Controller
{
    public $payment;

    public function index(Request $request) {
        $collection = collect(shopifyPlans());
        $filtered = $collection->first(function ($value, $key) use($request) {
            return $request->plan_id == $value['id'];
        });
        $shop = shopResource();
        $a = new  ShopifyPaymentGateway($shop);

        if(!is_null(@$request->charge_id)){
            $resp = $a->activateRecurringCharges($request->charge_id, null);
            if(@$resp['id']){
                $this->updatePlan($resp, $request->plan_id,'active');
            }
            return redirect()->route('shop.dashboard')->with('success', 'Subscribed successfully');
        }elseif($request->plan_id == 1) {

            $this->cancelActivePlan('pending');

            $userShop = userShop();
            $charge = new Charge;
            $charge->shop_id = $userShop->id;
            $charge->plan_id = $request->plan_id;
            $charge->type = 1;
            $charge->status = "active";
            $charge->name = $filtered['title'];
            $charge->price = $filtered['price'];
            $charge->save();

            $userShop->plan = $request->plan_id;
            $userShop->save();

            return redirect()->route('shop.dashboard')->with('success', 'Subscribed successfully');
        }
        elseif(@count($filtered)){

            $param = [
                'recurring_application_charge' => [
                    "name" =>  $filtered['title'],
                    "price" => $filtered['price'],
                    "return_url" => route("charge.payment",['plan_id' => $request->plan_id]),
                    "trial_days"=> 5,
                    "test" => true,
                ],
            ];

            $resp = $a->recurringCharges($param);
            if(@$resp['id']){
                $this->updatePlan($resp,$request->plan_id,'pending');
            }
            return redirect($resp['confirmation_url']);
        }
    }

    public function updatePlan($resp, $plan_id, $type = null)
    {
        $userShop = userShop();

        $this->cancelActivePlan($type);

        $charge = Charge::firstOrNew(
            [
                'charge_id' => @$resp['id'],
                'shop_id'   => $userShop->id,
            ]
        );

        $charge->plan_id = $plan_id;
        $charge->type = 1;
        $charge->test = @$resp['test'];
        $charge->status = @$resp['status'];
        $charge->name = @$resp['name'];
        $charge->price = @$resp['price'];
        $charge->trial_days = @$resp['trial_days'];
        $charge->billing_on = @$resp['billing_on'];
        $charge->activated_on = @$resp['activated_on'];
        $charge->trial_ends_on = @$resp['trial_ends_on'];
        $charge->cancelled_on = @$resp['cancelled_on'];
        //$charge->expires_on = $resp['expires_on'];
        $charge->save();

        if($type == 'active') {
            $userShop->plan = $plan_id;
            $userShop->save();
        }

    }

    public function cancelActivePlan($type){
        $shop = shopResource();
        $a = new  ShopifyPaymentGateway($shop);
        if($type == 'pending') {
            $charges = $a->getAllRecurringCharges();

            foreach ($charges as $key => $val){
                if($val->status == "active"){
                    $a->cancelRecurringCharge($val->id);

                    $oldCharge = Charge::where('charge_id', $val->id)->where('shop_id', $shop->id)->orderBy('id',"desc")->first();

                    $charge = new Charge;
                    $charge->charge_id = @$oldCharge->charge_id;
                    $charge->shop_id = $shop->id;
                    $charge->plan_id = @$oldCharge->plan_id;
                    $charge->type = 1;
                    $charge->status = "cancelled";
                    $charge->name = @$oldCharge->title;
                    $charge->price = @$oldCharge->price;
                    $charge->save();

                }
            }
        }
    }
}
