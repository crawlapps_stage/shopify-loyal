<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;

use App\User;
use App\Shop;
use App\EarningAction;
use App\EarningRule;
use App\CustomerLevel;
use App\CustomerLevelPoint;
use App\Customer;
use App\EarningDetail;

use Auth;
use Hash;

class Installation extends Controller
{
    public function install(Request $req)
    {
    	$shopUrl = $req->shop;
    	$shopExist = Shop::whereUrl($shopUrl)->first();
    	if($shopExist)
    	{
    		Auth::login($shopExist->user);
    		/*if ($shopExist->plan > 1 && !authUser()->subscribed('main')) {
			    $shopExist->plan = 0;
			    $shopExist->save();
			    return redirect()->route('choose.plan');
			}*/
    		return redirect()->route('shop.dashboard');
    	}

	    $scope = [
	    	"read_products","read_orders", "write_orders", "read_customers",
	    	"write_script_tags", "read_price_rules", "write_price_rules"
	    ];
	    $redirectUrl = route('process.oauth');
	    $authorizeUrl = Shopify::setShopUrl($shopUrl)
	    				->getAuthorizeUrl($scope,$redirectUrl);
	    return redirect()->to($authorizeUrl);
    }

    public function processOAuthResult(Request $req)
    {
		$accessToken = Shopify::setShopUrl($req->shop)->getAccessToken($req->code);

		$resource = Shopify::setShopUrl($req->shop)
				->setAccessToken($accessToken);

		try{
		    $shopifyShop = $resource->get("admin/shop.json")->toArray();
		    $locations = $resource->get("admin/locations.json")->toArray();
		}
		catch(\Exception $e){
		    dd('SOMETHING WENT WRONG!');
		}

		$owner = new User();
		$owner->name = $shopifyShop['name'];
		$owner->email = $shopifyShop['email'];
		$owner->password = str_shuffle(str_limit($accessToken, 12));
		$owner->role = 1;
		$owner->save();

		$shop = new Shop();
		$shop->user_id = $owner->id;
		$shop->url = $req->shop;
		$shop->currency = $shopifyShop['currency'];
		$shop->access_token = $accessToken;
		$shop->location_id = $locations[0]->id;
		$shop->save();

		$earningActions = EarningAction::all();
		foreach ($earningActions as $ea)
		{
			$earningRule = new EarningRule();
			$earningRule->shop_id = $shop->id;
			$earningRule->earning_action_id = $ea->id;
			$earningRule->points = $ea->default_points;
			if($ea->id == 5)
				$earningRule->points .= ',1';
			$earningRule->save();
		}

		$customerLevels = CustomerLevel::all();
		foreach ($customerLevels as $cl)
		{
			$clp = new CustomerLevelPoint();
			$clp->shop_id = $shop->id;
			$clp->required_points = $cl->default_points;
			$clp->title = $cl->title;
			$clp->save();
		}

		$this->initialAPICalls($resource, $shop->id);

		Auth::login($owner);
		return redirect()->route('shop.dashboard');
    }
    public function initialAPICalls($resource, $shopId)
    {
    	$routesPrefix = env('APP_URL').'/api/';
    	$resource->post(
			"admin/script_tags.json",
			[
				"script_tag" => [
					"event" => "onload",
					"src" => env('APP_URL').'/js/shopify.js'
				]
			]
		);
		$resource->post(
			"admin/webhooks.json",
			[
				"webhook" => [
					"topic"=> "orders/create",
				    "address"=> $routesPrefix.'order/create',
				    "format"=> "json"
				]
			]
		);
		$resource->post(
			"admin/webhooks.json",
			[
				"webhook" => [
					"topic"=> "orders/paid",
				    "address"=> $routesPrefix.'order/paid',
				    "format"=> "json"
				]
			]
		);

        $routesPrefix =  env('APP_URL').'/api/webhooks/';
        $resource->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "app/uninstalled",
                    "address"=> $routesPrefix.'app-uninstalled',
                    "format"=> "json"
                ]
            ]
        );

        $response = $resource->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "shop/update",
                    "address"=> $routesPrefix.'shop-update',
                    "format"=> "json"
                ]
            ]
        );

        /*$response = $resource->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "shop/redact",
                    "address"=> $routesPrefix.'shop-redact',
                    "format"=> "json"
                ]
            ]
        );*/

        /*$response = $resource->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "customers/redact",
                    "address"=> $routesPrefix.'customers-redact',
                    "format"=> "json"
                ]
            ]
        );*/

        /*$response = $resource->post(
            "admin/api/2020-04/webhooks.json",
            [
                "webhook" => [
                    "topic"=> "customers/data_request",
                    "address"=> $routesPrefix.'customers-data-request',
                    "format"=> "json"
                ]
            ]
        );*/
    }
}
