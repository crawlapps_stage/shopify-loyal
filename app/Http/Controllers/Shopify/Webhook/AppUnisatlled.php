<?php

namespace App\Http\Controllers\Shopify\Webhook;

use App\Campaign;
use App\CampaignItem;
use App\Charge;
use App\Customer;
use App\CustomerLevelPoint;
use App\DiscountRule;
use App\EarningDetail;
use App\EarningDetailProduct;
use App\EarningRule;
use App\Event;
use App\JumpConversion;
use App\JumpPoint;
use App\Shop;
use App\ShoppingRule;
use App\SpendingDetail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppUnisatlled extends Controller
{
    public function index(Request $request){
        $response = $this->verifyShopify($request);
        if($response['verified'])
        {
            $data = json_decode($response['data'],1);
            $shop = Shop::where('url',$data['domain'])->firstOrFail();
            $oldCharge = Charge::where('shop_id', $shop->id)->orderBy('id',"desc")->first();

            $this->deleteRelatedData($shop);
            $charge = new Charge;
            $charge->shop_id = $shop->id;
            $charge->charge_id = @$oldCharge->charge_id;
            $charge->plan_id = @$oldCharge->plan_id;
            $charge->type = 1;
            $charge->status = "cancelled";
            $charge->name = @$oldCharge->title;
            $charge->price = @$oldCharge->price;
            $charge->save();

            $user = User::find($shop->user_id);
            $user->delete();

            $shop->access_token = null;
            $shop->plan = null;
            $shop->save();
            $shop->delete();
        }
        return true;
    }

    public function deleteRelatedData($shop){
        \Log::info("---------- deleteRelatedData called ---------");
        Campaign::where('shop_id', $shop->id)->delete();
        CampaignItem::where('shop_id', $shop->id)->delete();
        Customer::where('shop_id', $shop->id)->delete();
        CustomerLevelPoint::where('shop_id', $shop->id)->delete();
        DiscountRule::where('shop_id', $shop->id)->delete();
        $detail = EarningDetail::where('shop_id', $shop->id)->get();
        EarningDetailProduct::whereIn('earning_detail_id',$detail->pluck('id')->toArray())->delete();
        EarningDetail::where('shop_id', $shop->id)->delete();
        EarningRule::where('shop_id', $shop->id)->delete();
        Event::where('shop_id', $shop->id)->delete();
        JumpConversion::where('shop_id', $shop->id)->delete();
        JumpPoint::where('shop_id', $shop->id)->delete();
        ShoppingRule::where('shop_id', $shop->id)->delete();
        SpendingDetail::where('shop_id', $shop->id)->delete();
    }

    public function shopRedact(Request $request)
    {
        \Log::info("---------- shopRedact called ---------");
        return true;
    }

    public function customersRedact(Request $request)
    {
        \Log::info("---------- customersRedact called ---------");
        return true;
    }

    public function customersDataRequest(Request $request)
    {
        \Log::info("---------- customersDataRequest called ---------");
        return true;
    }

    public function verifyShopify($request){
        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = verify_webhook($data, $hmac_header);
        return [ 'data' => $data, 'verified' => $verified ];
    }
}
