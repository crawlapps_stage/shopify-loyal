<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;

class OfferController extends Controller
{
    public function list()
    {
    	$offers = DB2()->select('select * from offer where companyId='.userShop()->company_id.' and shopify_shop_id='.userShopId());
    	return view('shop.offer.list', compact('offers'));
    }

    public function add()
    {
    	$formData = $this->getFormData();
    	return view('shop.offer.add', $formData);
    }

    public function edit($id)
    {
    	$offer = DB2()->select('select * from offer where id='.$id.' and shopify_shop_id='.userShopId());
    	if(!empty($offer[0]))
    	{
    		$offer = $offer[0];
    		$campaign = Campaign::find($offer->campaign_id);
    		$data = array_merge(compact('offer', 'campaign'), $this->getFormData());
    		return view('shop.offer.edit', $data);
    	}
    	else
    		return ['error', 'Offer not found!'];
    }

    public function save(Request $req, $id = null)
    {
    	$req->validate([
    		'name' => 'required|max:191',
    		'image' => 'max:1000',
    		'offer_categories' => 'required|min:1',
    		'campaign' => 'required|exists:campaigns,id',
    		'details' => 'required|max:191'
    	]);

    	$campaign = Campaign::find($req->campaign);
    	if($campaign->shop_id != userShopId())
    		return ['error', 'Campaign not found!'];

    	if(!is_null($id))
    	{
    		$offer = DB2()->select('select * from where id='.$id.' and shopify_shop_id='.userShopId());
    		if(empty($offer[0]))
    			return ['error', 'Offer not found!'];
    	}

    	if($req->image)
    		$image = uploadFile($req->image);
    	else
    		$image = '';

        error_reporting(0);
    	$offer = [
    		'name' => $req->name,
    		'campaign_id' => $campaign->id,
    		'shopify_shop_id' => userShopId(),
    		'imageUrl' => $image,
    		'startTime' => $campaign->starts_at . ':00',
    		'endTime' => $campaign->ends_at . ':00',
    		'loyalityRequired' => $campaign->required_points,
    		'targetYears' => $campaign->target_age_from . '-' . $campaign->target_age_to,
    		'targetGender' => $campaign->target_gender ?: 0,
    		'cuponCode' => $campaign->discount_code,
    		'cuponType' => $campaign->discount_type,
    		'discount' => $campaign->discount,
    		'details' => $req->details,
    		'companyId' => userShop()->company_id,
    		'offerCategories' => '|'.implode('|', $req->offer_categories).'|',
    		'offerHubs' => sizeof($req->offer_hubs) ? '|'.implode('|', $req->offer_hubs).'|' : '',
    		'creationTime' => date('Y-m-d H:i:s')
    	];

    	if(is_null($id))
    		DB2()->table('offer')->insert($offer);
    	else
    		DB2()->table('offer')->where('id', $id)->update($offer);

    	return ['success', 'Offer saved successfully'];
    }

    public function delete($id)
    {
    	$offer = DB2()->select('select id,shopify_shop_id from offer where id='.$id.' and shopify_shop_id='.userShopId());
    	if(!empty($offer[0]))
    	{
    		DB2()->where('id', $id)->delete();
    		return ['success', 'Offer deleted'];
    	}
    	else
    		return ['error', 'Offer not found!'];	
    }

    public function getFormData()
    {
    	$offerHubs = DB2()->select("select id as `key`, name as `value` from company where find_in_set(cast(id as CHAR),(select REPLACE(trim(BOTH '|' FROM availableOfferHubs),'|',',') as ids FROM company where company.id='".userShop()->company_id."'))");
	    
        $shop_categories = array();
        $res_shop_categories = DB2()->select("SELECT shopCategory FROM company WHERE id=".userShop()->company_id."");

        if(!empty($res_shop_categories))
        {
            $categories_string = trim($res_shop_categories[0]->shopCategory,'|');
			if($categories_string != ''){
				 $shop_categories = explode('|', $categories_string);
			}               
        }
        $offerCategories = DB2()->select("SELECT id as item_id,parentId as parent_id, name as name FROM shopcategory WHERE ".((!empty($shop_categories)) ? " id IN (".implode(',', $shop_categories).") AND " : "")." isActive=1 ORDER BY name");

        $campaigns = Campaign::whereShopId(userShopId())->get();
        return [
        	'offerHubs' => $offerHubs, 
        	'offerCategories' => $offerCategories,
        	'campaigns' => $campaigns
        ];
    }
}
