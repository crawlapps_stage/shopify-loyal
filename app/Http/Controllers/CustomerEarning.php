<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;

use App\Shop;
use App\Event;
use App\EarningDetail;
use App\EarningRule;
use App\SpendingDetail;
use App\CustomerLevelPoint;
use App\Customer;
use App\DiscountRule;
use App\EarningDetailProduct;

class CustomerEarning extends Controller
{
    public function order(Request $req)
    {
        error_reporting(0);
    	$order = json_decode($req->getContent(), true);
    	
        $check = EarningDetail::whereOrderId($order['id'])->first();
        if($check)
            return 'ALREADY INSERTED';
    	$customer = Customer::whereCustomerId($order['customer']['id'])->first();

    	if($customer)
    	{
    		$rule = EarningRule::where([
    			'shop_id' => $customer->shop_id, 
    			'earning_action_id' => 5
    		])->first();

    		list($points, $forEvery) = explode(',', $rule->points);
    		$earnedPoints = ($order['total_line_items_price'] / $forEvery) * $points;

    		$extraPointInfo = '';

    		//IS EVENT
    		$eventPoints = 0;
    		$event = Event::where([
	    		'shop_id' => $customer->shop_id, 'status' => 1
	    	])
	    	->where('starts_at' ,'<=', date('Y-m-d H:i:s'))
	    	->where('ends_at' ,'>=', date('Y-m-d H:i:s'))
	    	->with('extraPoints')->first();
	    	if($event)
	    	{
	    		$ep = $event->extraPoints->where('earning_rule_id', $rule->id);
	    		if(!empty($ep[0])){
	    			$eventPoints = $ep[0]->points * $earnedPoints;
	    			$extraPointInfo = $eventPoints . ' points for "' . strtoupper($event->name) . ' EVENT", ';
	    		}
	    	}

	    	//CHECK LEVEL
	    	$customerLevel = CustomerLevelPoint::find($customer->level_id);
	    	if($customerLevel)
	    	    $levelPoints = $customerLevel->extraPoint->points * $earnedPoints;
	    	else 
	    	    $levelPoints = 0;
	    	if($levelPoints)
	    	{
	    		$extraPointInfo .= $levelPoints . ' points for "' . strtoupper($customerLevel->title) . ' CUSTOMER LEVEL", ';
	    	}

	    	$ed = new EarningDetail();
	    	$ed->shop_id = $customer->shop_id;
	    	$ed->customer_id = $customer->id;
	    	$ed->earning_rule_id = $rule->id;
	    	$ed->order_id = (string)$order['id'];
	    	$ed->shopify_order_id = (string)$order['id'];
	    	$ed->status_url = $order['order_status_url'];
	    	$ed->points = $earnedPoints;
	    	$ed->extra_points = $eventPoints + $levelPoints;
	    	$ed->total_points = $ed->points + $ed->extra_points;
	    	$ed->occasion = $rule->earningAction->name;
	    	$ed->extra_point_info = ltrim($extraPointInfo, ', ');
	    	$ed->status = 0;
	    	$ed->spent_amount = $order['total_line_items_price'];
	    	$ed->total_amount = $order['total_price'];
	    	$ed->location_id = $order['location_id'];
	    	$ed->dot = date('d-m-Y');
	    	$ed->save();
	    	
	    	$edpCount = 1;
	    	foreach($order['line_items'] as $item)
	    	{
	    	    $edp = new EarningDetailProduct();
	    	    $edp->line_item_id = $item['id'];
	    	    $edp->earning_detail_id = $ed->id;
	    	    $edp->title = $item['title'];
	    	    $edp->item_number = $edpCount;
	    	    $edp->amount = $item['price'] * $item['quantity'];
	    	    $edp->save();
	    	    $edpCount++;
	    	}
	    	if(sizeof($order['discount_codes']))
	    	{
	    		$code = $order['discount_codes'][0]['code'];
	    		$discount = DiscountRule::whereShopId($customer->shop_id)->whereCode($code)->first();
	    		if($discount && $discount->discount != 0)
	    		{
			    	if($discount->discount_type == 1)
			    		$d = $discount->discount . '% off';
			    	else if($discount->discount_type == 2)
			    		$d = $customer->shop->currency . $discount->discount . ' off';
			    	else
			    		$d = 'Free Shipping';
			    	$ed->discount = $d;
			    	$ed->save();
	    		}
	    	}
    	}
    }
    public function orderPaid(Request $req)
    {
        $order = json_decode($req->getContent(), true);
        $earning = EarningDetail::whereOrderId((string)$order['id'])->first();
        if($earning)
        {
            $earning->status = 1;
            $earning->save();
            $customer = Customer::find($earning->customer_id);
            $customer->total_points += $earning->total_points;
            $customer->remaining_points += $earning->total_points;
            $customer->save();
            
            //SPENDING DETAIL
            if(sizeof($order['discount_codes']))
            {
                $code = $order['discount_codes'][0]['code'];
                $discount = DiscountRule::whereShopId($customer->shop_id)->whereCode($code)->first();
                $this->saveSpendingDetail($discount, $customer, $order['id']);
            }
            updateLevel($customer);
        }
    }
    public function csvData(Request $req)
    {
        $req = $this->getCSVData();
        $rule = EarningRule::where([
			'shop_id' => 34, 
			'earning_action_id' => 5
		])->first();
		$event = Event::where([
    		'shop_id' => 34, 'status' => 1
    	])
    	->where('starts_at' ,'<=', date('Y-m-d H:i:s'))
    	->where('ends_at' ,'>=', date('Y-m-d H:i:s'))
    	->with('extraPoints')->first();
    	$shop = Shop::find(34);
    	$refundLocationId = $shop->location_id;
    	list($points, $forEvery) = explode(',', $rule->points);
    	
        foreach($req['spendings']['total'] as $s)
        {
            if(strpos($s['barcode'], 'C') !== false && strpos($s['barcode'], 'CV') === false)
                $barcode = str_replace('C', '9800', $s['barcode']);
            else
                $barcode = $s['barcode'];
                
            $customer = Customer::where('barcode', $barcode)
            ->orWhere('plastic_card_barcode', $barcode)
            ->orWhere('plastic_card_barcode1', $barcode)->first();
            if($customer)
            {
                $updatePoints = null;
                
        		$earnedPoints = ($s['spent_amount'] / $forEvery) * $points;
        		$extraPointInfo = '';
    
        		//IS EVENT
        		$eventPoints = 0;
    	    	if($event)
    	    	{
    	    		$ep = $event->extraPoints->where('earning_rule_id', $rule->id);
    	    		if(!empty($ep[0])){
    	    			$eventPoints = $ep[0]->points * $earnedPoints;
    	    			$extraPointInfo = $eventPoints . ' points for "' . strtoupper($event->name) . ' EVENT", ';
    	    		}
    	    	}
    
    	    	//CHECK LEVEL
    	    	$customerLevel = CustomerLevelPoint::with('extraPoint')->whereId($customer->level_id)->first();
    	    	if(!empty($customerLevel) && !empty($customerLevel->extraPoint))
    	    	    $levelPoints = $customerLevel->extraPoint->points * $earnedPoints;
    	    	else 
    	    	    $levelPoints = 0;
    	    	if($levelPoints)
    	    	{
    	    		$extraPointInfo .= $levelPoints . ' points for "' . strtoupper($customerLevel->title) . ' CUSTOMER LEVEL", ';
    	    	}
    	    	if($s['discount_percentage'])
    	    	    $discounted = ((int)$s['discount_percentage']/100)*$s['spent_amount'];
    	    	else
    	    	    $discounted = 0;
    	    	
    	    	$check = EarningDetail::where('shopify_order_id', $s['shopify_order_id'])
                         ->orWhere('order_id', $s['order_id'])->first();
                $ed = $check ?: new EarningDetail();
    	    	$ed->shop_id = $customer->shop_id;
    	    	$ed->customer_id = $customer->id;
    	    	$ed->earning_rule_id = $rule->id;
    	    	$ed->order_id = (string)$s['order_id'];
    	    	$ed->points = $earnedPoints;
    	    	$ed->extra_points = $eventPoints + $levelPoints;
    	    	$ed->total_points = $ed->points + $ed->extra_points;
    	    	$ed->occasion = $rule->earningAction->name;
    	    	$ed->extra_point_info = ltrim($extraPointInfo, ', ');
    	    	$ed->spent_amount = $s['spent_amount'] - $discounted;
    	    	$ed->discount = $s['discount_percentage'];
    	    	$ed->dot = date('d-m-Y', strtotime($s['date']));
    	    	$ed->save();
    	    	
    	    	$refunds = [];
    	    	foreach($req['spendings']['details'][$s['order_id']] as $sD)
    	    	{
    	    	    $prevEdp = EarningDetailProduct::where([
    	    	        'earning_detail_id' => $ed->id,
    	    	        'item_number' => $sD['item_number'],
    	    	        'amount' => $sD['spent_amount']
    	    	    ])->first();
    	    	    $edp = $prevEdp ?: new EarningDetailProduct();
    	    	    $edp->earning_detail_id = $ed->id;
    	    	    $edp->item_number = $sD['item_number'];
    	    	    $edp->title = $sD['title'];
    	    	    $edp->amount = $sD['spent_amount'];
    	    	    if($prevEdp)
    	    	        $edp->status = 1;
    	    	    if($sD['status'] == 1)
    	    	        $edp->status = 2;
    	    	    if($sD['spent_amount'] < 0){
    	    	        $edp->status = 4;
    	    	        if($ed->status_url){
    	    	            $refunds[] = [
    	    	                'line_item_id' => $prevEdp->line_item_id,
    	    	                'quantity' => 1,
    	    	                'restock_type' => 'return',
    	    	                'location_id' => $refundLocationId
    	    	            ];
    	    	        }
    	    	    }
    	    	    $edp->save();
    	    	}
    	    	$statusCheck = $ed->edproducts()->where('status', '!=', 2)->get();
    	    	if(count($statusCheck) == 0){
    	    	    $ed->status = 1;
    	    	    $ed->save();
    	    	    
    	    	    $updatePoints = $ed;
    	    	    
    	    	    if($ed->status_url)
    	    	    {
    	    	        try{
    	    	            shopResource($shop)->post(
                        		"admin/orders/".$ed->shopify_order_id."/transactions.json", 
                    			[
                    			    "transaction" => [
                    			        "amount" => $ed->total_amount,
                    				    "kind"   => "capture"
                    			    ]
                    			]
                        	);
    	    	        }
    	    	        catch(\Exception $d){
    	    	            
    	    	        }
    	    	    }
    	    	}
    	    	if(sizeof($refunds)){
    	    	    shopResource($shop)->post(
        	    		"admin/orders/".$ed->shopify_order_id."/refunds.json", 
        				[
        				    "refund" => [
        				        "notify" => true,
            				    "note" => "Rufund by customer",
            				    "refund_line_items" => [
            				        $refunds
            				    ]
        				    ]
        				]
        	    	);
    	    	}
    	    	if($updatePoints != null)
    	    	{
    	    	    $customer->total_points += $updatePoints->total_points;
                    $customer->remaining_points += $updatePoints->total_points;
                    $customer->save();
                    
                    $discount = DiscountRule::where([
                        'discount_type' => 1,
                        'discount' => $s['discount_percentage'],
                        'shop_id' => $customer->shop_id
                    ])->first();
                    $this->saveSpendingDetail($discount, $customer, $updatePoints->order_id);
                    
        	    	$reachingLevel = $customer->level_id ? false : true;
        	    	updateLevel($customer, $reachingLevel);   
    	    	}
            }
        }
        echo "<script>alert('Done')</script>";
    }
    public function saveSpendingDetail($discount, $customer, $orderId)
    {
        if($discount && $discount->discount != 0)
		{
			$sd = new SpendingDetail();
			$sd->shop_id = $customer->shop_id;
	    	$sd->customer_id = $customer->id;
	    	$sd->discount_rule_id = $discount->id;
	    	$sd->spent_points = $discount->points;

	    	if($discount->discount_type == 1)
	    		$d = $discount->discount . '% off';
	    	else if($discount->discount_type == 2)
	    		$d = $customer->shop->currency . $discount->discount . ' off';
	    	else
	    		$d = 'Free Shipping';

	    	if($discount->based_on == 1)
	    		$sd->discount = $d . ' (on specific collection)';
	    	else
	    		$sd->discount = $d;

	    	$sd->order_id = $orderId;
			$sd->save();

			$customer->spending += $sd->spent_points;
			$customer->remaining_points -= $sd->spent_points;
			$customer->save();
		}
    }
    
    public function getCSVData(){
        $csvFile = fopen(\Storage::disk('local')->path('h-testing.csv'), 'r');
        $spendings = [];
        $added = [];
        fgetcsv($csvFile);
        while(($line = fgetcsv($csvFile, 1000000, "\t")) !== FALSE){
        	if($line[4] == 'GLS' || $line[4] == 'PPP')
        	    continue;
        	$line[4] = trim(str_replace("_", "", $line[4]));
           $spendings['details'][$line[0]][] = [
               'title' => utf8_encode($line[5]),
               'spent_amount' => $line[8],
               'item_number' => $line[2],
               'status' => $line[14]
           ];
           if(!in_array($line[0], $added))
           {
               $spendings['total'][$line[0]] = [
                   'order_id' => $line[0],
                   'barcode' => $line[3],
                   'date' => $line[11],
                   'discount_percentage' => $line[9],
                   'spent_amount' => (double)$line[8],
                   'shopify_order_id' => $line[15]
               ];
               $added[] = $line[0];
           }
           else
           {
               $spendings['total'][$line[0]]['spent_amount']
               += (double)$line[8];
           }
        }
        return compact('spendings');
    }
}
