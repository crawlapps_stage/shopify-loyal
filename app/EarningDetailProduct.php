<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EarningDetailProduct extends Model
{
    public function earningDetail()
    {
    	return $this->belongsTo('App\EarningDetail');
    }
}
