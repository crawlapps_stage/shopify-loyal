<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function customers()
    {
    	return $this->hasMany('App\Customer');
    }
    public function discountRules()
    {
    	return $this->hasMany('App\DiscountRule');
    }
}
