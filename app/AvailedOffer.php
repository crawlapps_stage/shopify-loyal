<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailedOffer extends Model
{
    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }
    public function campaignItem()
    {
        return $this->belongsTo('App\CampaignItem');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
