<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EarningRule extends Model
{
    public function earningAction()
    {
    	return $this->belongsTo('App\EarningAction');
    }
}
