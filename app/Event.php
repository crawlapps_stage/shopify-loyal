<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function extraPoints()
    {
    	return $this->hasMany('App\JumpPoint', 'based_on_id')->where('based_on', 2);
    }
    public function discounts()
    {
    	return $this->hasMany('App\DiscountRule', 'based_on_id')->where('based_on', 2);
    }
}
