<?php 

namespace App\Traits;

use App\DiscountRule;
use App\Event;
use App\CustomerLevelPoint;
use App\Customer;

trait DiscountSetup{
	public static function setup($req, $id = null)
    {
    	$per = $req->per;
    	if($per == 'campaign')
    	{
    		$per = 1;
    	}
    	else if($per == 'general')
    	{
    		$per = 3;
    	}
	    else
	    {
	    	if($per == 'event')
	    	{
	    		$basedOn = Event::whereShopId(userShopId());
	    		$per = 2;
	    	}
	    	else
	    	{
	    		$basedOn = CustomerLevelPoint::whereShopId(userShopId());
	    		$per = 3;
	    	}
	    	if(empty($basedOn->whereId($req->based_on_id)->first()))
	    		return 'FAILED';
	    }

    	if(is_null($id))
    	{
	    	$discount = new DiscountRule();
	    	$coupon = $req->coupon_code; //generateCoupon();

	    	list($priceRuleId, $discountCodeId) = self::generateDiscountRule($req, null, $coupon);
    	}
	    else
	    {
	    	$discount = DiscountRule::find($id);
	    	if(empty($discount))
	    		return 'FAILED';

	    	$coupon = $req->coupon_code; //$discount->code;

	   // 	if((float)$req->discount == (float)$discount->discount && $req->discount_type == $discount->discount_type)
	   // 	{
	   // 		$sameRule = true;
	   // 		$priceRuleId = $discount->price_rule_id;
	   // 		$discountCodeId = $discount->discount_code_id;
	   // 	}
	   // 	else
	   // 	{
	   // 		list($priceRuleId, $discountCodeId) = self::generateDiscountRule($req, $discount, $coupon);
	   // 	}
	        list($priceRuleId, $discountCodeId) = self::generateDiscountRule($req, $discount, $coupon);
	    }
	    $discount->shop_id = userShopId();
	    $discount->based_on = $per;
	    $discount->based_on_id = $req->based_on_id;
	    $discount->points = $req->points ?: 0;
	    $discount->discount = $req->discount;
	    $discount->discount_type = $req->discount_type;
	    $discount->discount_code_id = $discountCodeId;
	    $discount->price_rule_id = $priceRuleId;
	    $discount->code = $coupon;
	    $discount->save();

	    return 'OK';
    }
    public static function generateDiscountRule($req, $discount, $coupon)
    {
    	$priceRule = [
	    	"title" => $coupon,
		    "target_type" => $req->discount_type != '3' ? "line_item" : 'shipping_line',
		    "target_selection" => $req->per != 'campaign' ? "all" : 'entitled',
		    "allocation_method" => $req->discount_type != '3' ? "across" : 'each',
		    "customer_selection" => "prerequisite",
		    "starts_at" => date('Y-m-d').'T'.date('h:s:i').'Z',
	    ];
	    if($req->per == 'campaign')
	    {
	    	$priceRule['entitled_product_ids'] = $req->products;
	    	$preCustomersCond = [
	    		'shop_id' => userShopId()
	    	];
	    	if($req->target_level)
	    		$preCustomersCond['level_id'] = $req->target_level;
	    	$preCustomers = Customer::where($preCustomersCond)
	    	->get()->pluck('customer_id')->toArray();
	    	$priceRule["prerequisite_customer_ids"] = sizeof($preCustomers) ? $preCustomers : [0];
	    }
	    else
	    {
	        $preCustomers = Customer::where([
	            'shop_id' => userShopId(), 'level_id' => $req->based_on_id
	        ])->get()->pluck('customer_id')->toArray();
	        $priceRule["prerequisite_customer_ids"] = sizeof($preCustomers) ? $preCustomers : [0];
	    }
	    if($req->discount_type != 2)
	    	$priceRule['value_type'] = 'percentage';
	    else
	        $priceRule['value_type'] = 'fixed_amount';
	    $priceRule['value'] = $req->discount_type == 3 ? '-100.0' : '-'.$req->discount;
	    $discountCode = ['code' => $coupon];
	    if(is_null($discount))
	    {
	    	$priceRuleResult = shopResource()->post(
	    		"admin/price_rules.json", 
				[
					"price_rule" => $priceRule
				]
	    	)->toArray();
	    	$discountCodeResult = shopResource()->post(
	    		"admin/price_rules/".$priceRuleResult['id']."/discount_codes.json", 
				[
					"discount_code" => $discountCode
				]
	    	)->toArray();
	    }
	    else
	    {
	    	$priceRule['id'] = $discount->price_rule_id;
	    	$discountCode['id'] = $discount->discount_code_id;
	    	
	    	$priceRuleResult = shopResource()->put(
	    		"admin/price_rules/".$priceRule['id'].".json", 
				[
					"price_rule" => $priceRule
				]
	    	)->toArray();
	    	$discountCodeResult = shopResource()->put(
	    		"admin/price_rules/".$priceRuleResult['id']."/discount_codes/".$discountCode['id'].".json", 
				[
					"discount_code" => $discountCode
				]
	    	)->toArray();
	    }

	    return [(string)$priceRuleResult['id'], (string)$discountCodeResult['id']];
    }
}

?>