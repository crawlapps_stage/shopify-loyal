<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLevelPoint extends Model
{
    public function customerLevel()
    {
    	return $this->belongsTo('App\CustomerLevel');
    }
    public function extraPoint()
    {
    	return $this->hasOne('App\JumpPoint', 'based_on_id')->where('based_on', 3);
    }
    public function generalDiscount()
    {
    	return $this->hasOne('App\DiscountRule', 'based_on_id')
		->where(['based_on' => 3, 'points' => 0]);
    }
}
