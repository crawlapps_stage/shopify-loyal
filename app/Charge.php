<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    protected $fillable = [
        'id',
        'charge_id',
        'plan_id',
        'test',
        'status',
        'name',
        'terms',
        'type',
        'price',
        'capped_amount',
        'trial_days',
        'billing_on',
        'activated_on',
        'trial_ends_on',
        'cancelled_on',
        'expires_on',
        'shop_id'
    ];
}
