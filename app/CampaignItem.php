<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignItem extends Model
{
    public function campaign()
    {
    	return $this->belongsTo('App\Campaign');
    }
}
