<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountRule extends Model
{
    public function campaign()
    {
    	return $this->belongsTo('App\Campaign', 'based_on_id');
    }
    public function event()
    {
    	return $this->belongsTo('App\Event', 'based_on_id');
    }
    public function customerLevelPoint()
    {
    	return $this->belongsTo('App\CustomerLevelPoint', 'based_on_id');
    }
}
