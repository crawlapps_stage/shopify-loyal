<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function level()
    {
    	return $this->belongsTo('App\CustomerLevelPoint', 'level_id');
    }
    public function shop()
    {
    	return $this->belongsTo('App\Shop');
    }
}
