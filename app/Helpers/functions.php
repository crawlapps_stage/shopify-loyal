<?php

use Auth as Auth;
use Log as Log;
use Mail as Mail;
use DB as DB;
use Oseintow\Shopify\Facades\Shopify;

use App\DiscountRule;
use App\EarningRule;
use App\EarningDetail;
use App\Event;
use App\CustomerLevelPoint;
use App\JumpPoint;
use App\Customer;
use App\Shop;

if(!function_exists('authUser'))
{
	function authUser($checkOnly = false)
	{
	    if(strpos($_SERVER['REQUEST_URI'], 'loyal-customer') !== false && !empty($_GET['customer']))
	        $user = Customer::whereCustomerId($_GET['customer'])->first();
	    else
	        $user =  Auth::user();

	    if($user)
	        return $user;
	    if($checkOnly)
	        return false;
	    dd('AUTHORIZATION FAILED!');
	}
}
if(!function_exists('userId'))
{
	function userId()
	{
		return authUser()->id;
	}
}
if(!function_exists('userShop'))
{
	function userShop()
	{
		return authUser()->shop;
	}
}
if(!function_exists('userShopId'))
{
	function userShopId()
	{
		return userShop()->id;
	}
}
if(!function_exists('shopResource'))
{
	function shopResource($shop = null)
	{
		$userShop = $shop == null ? userShop() : $shop;
		return Shopify::setShopUrl($userShop->url)
				->setAccessToken($userShop->access_token);
	}
}
if(!function_exists('generateCoupon'))
{
	function generateCoupon()
	{
		$coupon = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz'), 0, 32);
		if(DiscountRule::whereCode($coupon)->first())
			return generateCoupon();
		return $coupon;
	}
}
if(!function_exists('spendingRule'))
{
	function spendingRule($shopId = null)
	{
		$shopId = $shopId ? $shopId : userShopId();
		return explode(',', EarningRule::where(
			['shop_id' => $shopId, 'earning_action_id' => 5]
		)->first()->points);
	}
}
if(!function_exists('insertJumpPointRule'))
{
	function insertJumpPointRule($points, $basedOn, $basedOnId, $earningActionId)
	{
		$earningRule = EarningRule::where([
		    'shop_id' => userShopId(), 'earning_action_id' => $earningActionId
		])->first();
		$extraPoint = JumpPoint::whereShopId(userShopId())->where([
    		'based_on' => $basedOn, 'based_on_id' => $basedOnId, 'earning_rule_id' => $earningRule->id
    	])->first() ?: new JumpPoint();
		$extraPoint->shop_id = userShopId();
		$extraPoint->earning_rule_id = $earningRule->id;
		$extraPoint->based_on = $basedOn;
		$extraPoint->based_on_id = $basedOnId;
		$extraPoint->points = $points ?: 0;
		$extraPoint->save();
	}
}


/** EXCEPTIONS **/
if(!function_exists('notFound'))
{
	function notFound($object, $route, $routeParams = '')
	{
		return ['error', $object . ' not found!'];
	}
}
if(!function_exists('somethingWrong'))
{
	function somethingWrong($route, $routeParams = '')
	{
		return ['error', 'Something went wrong, please try again.'];
	}
}
if(!function_exists('TR'))
{
    function TR($key, $checkOnly = false)
    {
        $hu = [
            // "Users"=>"Felhasználók",
            // "Reward Program"=>"Hűségprogram",
            // "Hi"=>"Kedves",
            // "You Have"=>"Aktuális Forint egyenleged",
            // "Earn Points"=>"Vásárolj minél többet",
            // "All Levels"=>"Elérhető szintek",
            // "Shopping"=>"Vásárlás",
            // "Spend Points"=>"Költsd el forintjaid",
            // "General Discounts"=>"Kedvezmény",
            // "Event"=>"Esemény",
            // "My Rewards"=>"Jutalmaim",
            // "My Spendings"=>"Vásárlásaim",
            // "Order"=>"Rendelés",
            // "Spent"=>"Költés",
            // "Discount"=>"Kedvezmény",
            // "Admin"=>"Admin",
            // "Home"=>"Főoldal",
            // "Customer"=>"Vevő",
            // "All Customers"=>"Minden vevő",
            // "Name"=>"Név",
            // "Email"=>"Email",
            // "DoB"=>"Szül. Év.",
            // "Gender"=>"Nem",
            // "Points"=>"Forintok",
            // "Balance"=>"Egyenleg",
            // "Joined at"=>"Csatlakozott",
            // "Actions"=>"Cselekedeteim",
            // "Earnings"=>"Keresett",
            // "Spending"=>"Költött",
            // "Display"=>"Kijelzés",
            // "Customer Levels"=>"Vásárlói szintek",
            // "Customer Levels"=>"Vásárlói szintek",
            // "Required Points"=>"Szükséges forint",
            // "Extra Reward Points"=>"Extra jutalom forint",
            // "Discount Type"=>"Kedvezmény jellege",
            // "Percentage"=>"Százalék",
            // "Fixed"=>"Fix",
            // "Free Shipping"=>"Ingyen szállítás",
            // "Campaigns"=>"Kampányok",
            // "Reward Program"=>"Hűségprogram",
            // "Events"=>"Események",
            // "Discounts"=>"Kedvezmények",
            // "per Event"=>"Eseményenként",
            // "Discount Rules"=>"Kedvezmény szabályzat",
            // "Event Name"=>"Esemény neve",
            // "Required Points"=>"Szükséges egyenleg",
            // "Discount Type"=>"Kedvezmény jellege",
            // "Discount Value"=>"Kedvezmény értéke",
            // "Status"=>"Státusz",
            // "Actions"=>"Cselekedetek",
            // "Per Customer Level"=>"Vevő szintű",
            // "Add New"=>"Új létrehozása",
            // "Dashboard"=>"Dashboard",
            // "Campaigns"=>"Kampányok",
            // "Customers"=>"Vevők",
            // "Availed Offers"=>"Elfogadott Ajánlatok",
            // "Recent Customers"=>"Legutóbbi Vevők",
            // "Recently Availed Offers"=>"Legutóbbi Ajánlatok",
            // "Points Required" => "forint szükséges",
            // "Add Barcode" => "Hűségkártya Azonosító Hozzáadása",
            // "Add Additional Barcode" => "Hűségkártya A Második Azonosító Hozzáadása",
            // "Enter 13 digit barcode" => "Társkártya azonosítód",
            // "Enter 13 digit additional barcode" => "Amennyiben igényeltél társkártyát, kérjük itt add meg annak 13 számjegyű azonosítóját!",
            // "Link Barcode" => "Hűségkártya azonosító mentése",
            // "Create a store account and start earning rewards" => "Jelentkezz be vagy hozz létre egy fiókot és kezdd el gyűjteni a forintjaidat!",
            // "Create Store Account" => "Fiók létrehozása",
            // "Log In" => "Bejelentkezés",
            // "Reward Club" => "HűségKlub",
            // "Pending" => "Függőben",
            // "Barcode has been successfully linked with your account" => "Sikeresen mentettük hűségkártya azonosítód!",
            // "Your Barcode" => "Hűségkártya azonosítód",
            // "Your Additional Barcode" => "Társkártya azonosítód",
            // "Join Reward Program & Start Earning Now" => "Csatlakozz a HűségKlub-ba és gyűjtsd a forintokat!",
            // "Join Now" => "Csatlakozz Most",
            // "Something went wrong, please try again" => "Valami baj történt, próbálkozz újra",
            // "Thank you for joining" => "Köszönjük, hogy csatlakozott",
            // "Barcode" => "Hűségkártya azonosító",
            // 'Invalid Barcode' => 'Nem megfelő a Hűségkártya Azonosító',
            // 'I accept terms & conditions' => 'Elfogadom a HűségKlub szabályzatát',
            // 'Accept terms & conditions to continue' => 'Fogadd el a HűségKlub szabályzatát hogy csatlakozhass',
            // 'Download mobile app first' => 'Először töltse le a mobilalkalmazást',
            // 'Loyalty card code is optional and not required. You only need to get a Loyalty Card in the store.If you cannott find a problem now, you can add it to your account later in up to two cases' => 'Amennyiben rendelkezel Játéksziget plasztik kártyával, annak 13 számjegyű azonosítóját itt add meg! A kártya önmagában nem szükséges a hűségprogramban való elinduláshoz, arra abban esetben lesz szükséged, ha vásárlásaidat webáruházunkban és üzleteinkben felváltva bonyolítod. A kártya kódját a későbbiekben is a fiókodhoz rendelheted.',
            // 'You can add one more barcode' => 'Akésőbbiekben lehetőséged lesz társkártya hozzáadására',
            // 'Product' => 'Termék',
            // 'Price' => 'Ár',
            // 'View Order' => 'Részletek',
            // 'More Info' => 'Több információ',
            // 'Details' => 'Részletek',
            // 'Prev Purchases' => 'Korábbi vásárlásaim',
            // 'Pending Purchases' => 'Függőben lévő vásárlásaim'
        ];
        if(array_key_exists($key, $hu))
                return $hu[$key];
        if($checkOnly)
            return false;
        return $key;
    }
}
if(!function_exists('updateLevel'))
{
    function updateLevel($customer, $reachingLevel = true)
    {
    	$newLevel = CustomerLevelPoint::where([
            'shop_id' => $customer->shop_id
        ])->where('required_points', '<=', $customer->remaining_points)
        ->orderBy('required_points', 'desc')->first();
        if($newLevel)
        {
            if($customer->level_id != $newLevel->id)
            {
                $prevCustomerLevel = $customer->level_id;
                if($reachingLevel && ($customer->level->required_points < $newLevel->required_points))
                    saveEarning($customer, $customer->shop_id, 4);
                $customer->level_id = $newLevel->id;
                $customer->save();
                changeCustomerSelection($customer->shop_id, $newLevel->id);
                if(!is_null($prevCustomerLevel))
                    changeCustomerSelection($customer->shop_id, $prevCustomerLevel);
            }
        }
    }
}
if(!function_exists('changeCustomerSelection'))
{
    function changeCustomerSelection($shopId, $levelId)
    {
        $discount = DiscountRule::where([
            'based_on' => 3, 'based_on_id' => $levelId,
            'shop_id' => $shopId
        ])->first();
        if($discount)
        {
            try{
                $preCustomers = Customer::where([
                    'shop_id' => $shopId, 'level_id' => $levelId
                ])->get()->pluck('customer_id')->toArray();
                $priceRule = [];
                $priceRule["prerequisite_customer_ids"] = sizeof($preCustomers) ? $preCustomers : [0];
                $priceRule["customer_selection"] = "prerequisite";
                shopResource(Shop::find($shopId))->put(
    	    		"admin/price_rules/".$discount->price_rule_id.".json",
    				[
    					"price_rule" => $priceRule
    				]
    	    	);
            }
            catch(\Exception $e){

            }
        }
    }
}
if(!function_exists('saveEarning'))
{
    function saveEarning($customer, $shopId, $earningActionId)
    {
        $rule = EarningRule::where([
            'shop_id' => $shopId, 'earning_action_id' => $earningActionId
        ])->first();
        $ed = new EarningDetail();
        $ed->shop_id = $shopId;
        $ed->customer_id = $customer->id;
        $ed->earning_rule_id = $rule->id;
        $ed->points = $rule->points;
        $ed->total_points = $ed->points;
        $ed->occasion = $rule->earningAction->name;;

        $event = Event::where([
    		'shop_id' => $shopId, 'status' => 1
    	])
    	->where('starts_at' ,'<=', date('Y-m-d H:i:s'))
    	->where('ends_at' ,'>=', date('Y-m-d H:i:s'))
    	->with('extraPoints')->first();
    	if($event)
    	{
    		$ep = $event->extraPoints->where('earning_rule_id', $rule->id);
    		if(!empty($ep[0])){
    			$ed->extra_points = $ep[0]->points + $rule->points;
    			$ed->extra_point_info = $ed->extra_points . ' points for "' . strtoupper($event->name) . ' EVENT"';
    		}
    	}

        $ed->save();
        $customer->total_points += $ed->total_points;
        $customer->remaining_points += $ed->total_points;
        $customer->save();
    }
}
if(!function_exists('sendMail'))
{
    function sendMail($data)
    {
        Mail::send('email.'.$data['blade'], ['data' => $data], function($message) use ($data) {
             $message->to($data['to']);
             $message->subject($data['subject']);
        });
    }
}
if(!function_exists('DB2'))
{
    function DB2()
    {
        return DB::connection('mysql2');
    }
}
if(!function_exists('uploadFile'))
{
    function uploadFile($file, $path = 'offer_images')
    {
        $name = time().'_'.str_replace(' ', '-', $file->getClientOriginalName());
        $file->move($path,$name);
        return asset($path.'/'.$name);
    }
}

function revertSlug($string){
    return ucfirst(str_replace('-', ' ', $string));
}

function shopifyPlans(){
    return [
        'loyalty_starter' =>[
            'id' => 1,
            'price' => 0,
            'title' => 'Loyalty Starter',
        ],
        'online_loyalty' =>[
            'id' => 2,
            'price' => 49,
            'title' => 'Online Loyalty',
        ],
        'online_offline' =>[
            'id' => 3,
            'price' => 199,
            'title' => 'Online and Offline',
        ],
        'enterprise' =>[
            'id' => 4,
            'price' => 499,
            'title' => 'Enterprise',
        ]
    ];
}

function verify_webhook($data, $hmac_header)
{
    \Log::info('in4');
    $calculated_hmac = base64_encode(hash_hmac('sha256', $data, env('SHOPIFY_SECRET'), true));
    return hash_equals($hmac_header, $calculated_hmac);
}
?>
