<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EarningDetail extends Model
{
    public function edproducts()
    {
        return $this->hasMany('App\EarningDetailProduct', 'earning_detail_id');
    }
    public function shop(){
        return $this->belongsTo('App\Shop');
    }
}
