<?php
namespace App\Services;

use Oseintow\Shopify\Shopify;

class ShopifyPaymentGateway
{
    /**
     * @var Shopify
     */
    private $shopify;

    public function __construct(Shopify $shopify)
    {
        $this->shopify = $shopify;
    }

    public function initPayment(array $store)
    {
        $this->shopify->setShopUrl($store['website'])->setAccessToken($store['access_token']);

        return $this;
    }

    public function oneTimeCharge($params = null)
    {
        return $this->shopify->post('/admin/application_charges.json', $params);
    }

    public function getOneTimeCharge($chargeId)
    {
        return $this->shopify->get('/admin/application_charges/' . $chargeId . '.json');
    }

    public function activateOneTimeCharge($chargeId, $params = null)
    {
        return $this->shopify->post('/admin/application_charges/' . $chargeId . '/activate.json', $params);
    }

    public function getAllOneTimeCharges()
    {
        return $this->shopify->get('/admin/application_charges.json');
    }

    public function getAllOneTimeChargeSinceId($chargeId)
    {
        return $this->shopify->get('/admin/application_charges.json?since_id=' . $chargeId);
    }

    public function recurringCharges($params = null)
    {
        return $this->shopify->post('/admin/recurring_application_charges.json',$params);
    }

    public function activateRecurringCharges($chargeId, $params = null)
    {
        return $this->shopify->post('/admin/recurring_application_charges/' . $chargeId . '/activate.json', $params);
    }

    public function getSingleRecurringCharge($chargeId)
    {
        return $this->shopify->get('/admin/recurring_application_charges/' . $chargeId . '.json');
    }

    public function getAllRecurringCharges($field = "")
    {
        return $this->shopify->get('/admin/recurring_application_charges.json?'.$field);
    }

    public function getAllRecurringChargesSinceSpecificId($chargeId)
    {
        return $this->shopify->get('/admin/recurring_application_charges.json?since_id=' . $chargeId);
    }

    public function cancelRecurringCharge($chargeId)
    {
        return $this->shopify->delete('/admin/recurring_application_charges/' . $chargeId . '.json');
    }
}
