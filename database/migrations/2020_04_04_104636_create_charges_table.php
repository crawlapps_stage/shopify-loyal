<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->increments('id');

            // Filled in when the charge is created, provided by shopify, unique makes it indexed
            $table->bigInteger('charge_id')->unique()->nullable();
            $table->integer('plan_id')->unsigned()->nullable();

            // Test mode or real
            $table->boolean('test')->default(false)->nullable();

            $table->string('status')->nullable();

            // Name of the charge (for recurring or one time charges)
            $table->string('name')->nullable();

            // Terms for the usage charges
            $table->string('terms')->nullable();

            $table->integer('type')->nullable();

            // Store the amount of the charge, this helps if you are experimenting with pricing
            $table->decimal('price', 8, 2)->nullable();

            // Store the amount of the charge, this helps if you are experimenting with pricing
            $table->decimal('capped_amount', 8, 2)->nullable();

            // Nullable in case of 0 trial days
            $table->integer('trial_days')->nullable();

            // The recurring application charge must be accepted or the returned value is null
            $table->timestamp('billing_on')->nullable();

            // When activation happened
            $table->timestamp('activated_on')->nullable();

            // Date the trial period ends
            $table->timestamp('trial_ends_on')->nullable();

            // Not supported on Shopify's initial billing screen, but good for future use
            $table->timestamp('cancelled_on')->nullable();

            // Provides created_at && updated_at columns
            $table->timestamps();

            $table->timestamp('expires_on')->nullable();

            // Allows for soft deleting
            $table->softDeletes();

            // Linking
            $table->integer('shop_id')->unsigned();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
