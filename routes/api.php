<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function(){
    Route::group(['prefix' => "webhooks"], function(){
        Route::post("app-uninstalled","Shopify\Webhook\AppUnisatlled@index");
        Route::post("shop-redact","Shopify\Webhook\AppUnisatlled@shopRedact");
        Route::post("customers-redact","Shopify\Webhook\AppUnisatlled@customersRedact");
        Route::post("customers-data-request","Shopify\Webhook\AppUnisatlled@customersDataRequest");
        //Route::post("shop-update","Shopify\Webhook\AppUnisatlled@index");
    });

    Route::get('csv-data', 'CustomerEarning@csvData');
    Route::post('get-levels', 'CustomerController@getLevels');
    Route::group(['middleware' => 'webhooks'], function(){
        Route::post('customer/create', 'CustomerEarning@signup');
    	Route::post('customer/delete', 'CustomerController@onDelete');
    	Route::post('order/create', 'CustomerEarning@order');
    	Route::post('order/paid', 'CustomerEarning@orderPaid');
    });
});

