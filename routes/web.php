<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('subscribe', 'ShopController@subscribe')->name('subscribe');
Route::group(['middleware' => 'installation'], function(){
	Route::get('install', 'Installation@install')->name('install');
	Route::get('process-oauth-result', 'Installation@processOAuthResult')->name('process.oauth');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('charge', 'ChargeController@index')->name('charge.payment');
	Route::get('choose-plan', 'ShopController@choosePlan')->name('choose.plan');
//	Route::get('go-free', 'ShopController@goFree')->name('go.free');
});
Route::group(['prefix' => 'shop', 'middleware' => ['auth', 'subscribed', 'ajax']], function(){
	Route::get('dashboard', 'ShopController@index')->name('shop.dashboard');
	Route::get('enable', 'ShopController@enable')->name('enable');
	Route::get('disable', 'ShopController@disable')->name('disable');

	Route::get('display', 'ShopController@display')->name('display');
	Route::post('update-display', 'ShopController@updateDisplay')->name('update.display');

    Route::group(['prefix' => 'earning-rules'], function () {
	    Route::get('', 'EarningController@index')->name('earning.rules');
	    Route::get('update-status/{id}/{status}', 'EarningController@updateStatus')->name('update.earning.status');
	    Route::post('update', 'EarningController@update')->name('update.earning');
	    Route::get('{id}', 'EarningController@edit')->name('edit.earning');
	});
	Route::group(['prefix' => 'events'], function () {
	    Route::get('', 'EventController@index')->name('events');
	    Route::get('add', 'EventController@add')->name('add.event');
	    Route::get('update-status/{id}/{status}', 'EventController@updateStatus')->name('update.event.status');
	    Route::post('save/{id?}', 'EventController@save')->name('save.event');
	    Route::get('delete/{id}', 'EventController@delete')->name('delete.event');
	    Route::get('{id}', 'EventController@edit')->name('edit.event');
	});
	Route::group(['prefix' => 'customer-level'], function () {
		Route::get('', 'ShopController@customerLevels')->name('customer.levels');
	    Route::post('save', 'ShopController@updateCustomerLevels')->name('save.customer.levels');
	});
	Route::group(['prefix' => 'customers'], function () {
		Route::get('', 'ShopController@customers')->name('customers');
	    Route::get('earnings/{id}', 'ShopController@customerEarnings')->name('shop.customer.earnings');
	    Route::get('spendings/{id}', 'ShopController@customerSpendings')->name('shop.customer.spendings');
	});
	Route::group(['prefix' => 'campaign', 'middleware' => 'premium'], function () {
	    Route::get('', 'CampaignController@index')->name('campaigns');
	    Route::get('add', 'CampaignController@add')->name('add.campaign');
	    Route::get('update-status/{id}/{status}', 'CampaignController@updateStatus')->name('update.campaign.status');
	    Route::post('save/{id?}', 'CampaignController@save')->name('save.campaign');
	    Route::get('delete/{id}', 'CampaignController@delete')->name('delete.campaign');
	    Route::get('availed-items/{id}', 'CampaignController@availedItems')->name('availed.items');
	    Route::get('item-availers/{id}', 'CampaignController@itemAvailers')->name('item.availers');
	    Route::get('delete/{id}', 'CampaignController@delete')->name('delete.campaign');
	    Route::get('{id}', 'CampaignController@edit')->name('edit.campaign');
	});
	Route::group(['prefix' => 'offer', 'middleware' => 'premium'], function(){
		Route::get('list', 'OfferController@list')->name('offers');
		Route::get('add', 'OfferController@add')->name('add.offer');
		Route::get('edit/{id}', 'OfferController@edit')->name('edit.offer');
		Route::get('delete/{id}', 'OfferController@delete')->name('delete.offer');
		Route::post('save/{id?}', 'OfferController@save')->name('save.offer');
	});
	Route::group(['prefix' => 'discounts'], function () {
	    Route::get('{per}', 'Discounts@index')->name('discounts');
	    Route::get('add/{per}', 'Discounts@add')->name('add.discount');
	    Route::get('update-status/{id}/{status}', 'Discounts@updateStatus')->name('update.discount.status');
	    Route::post('save/{id?}', 'Discounts@save')->name('save.discount');
	    Route::get('delete/{per}/{id}', 'Discounts@delete')->name('delete.discount');
	    Route::get('edit/{per}/{id}', 'Discounts@edit')->name('edit.discount');
	});

	Route::group(['prefix' => 'offline-data'], function(){
		Route::post('upload', 'CustomerEarning@upload')->name('upload.csv');
	});
});
Route::group(['prefix' => 'loyal-customer', 'middleware' => 'cors'], function(){
	Route::get('', 'CustomerController@index');
	Route::get('info', 'CustomerController@info')->name('customer.learnmore');
	Route::get('join', 'CustomerController@join')->name('customer.join');
	Route::get('home', 'CustomerController@home')->name('customer.index');
	Route::get('save-app-user', 'CustomerController@saveAppUser');

	Route::group(['middleware' => ['web', 'customer']], function(){
		Route::get('dashboard', 'CustomerController@dashboard')->name('customer.dashboard');
		Route::get('earn', 'CustomerController@earn')->name('customer.earn');
		Route::get('earnings', 'CustomerController@earnings')->name('customer.earnings');
		Route::get('spend', 'CustomerController@spend')->name('customer.spend');
		Route::get('spendings', 'CustomerController@spendings')->name('customer.spendings');
		Route::get('spendings/{id}', 'CustomerController@spendingDetails')->name('spending.details');

		Route::get('levels', 'CustomerController@loyaltyLevels')->name('customer.loyalty.levels');

		Route::get('add-barcode', 'CustomerController@addBarcode')->name('add.barcode');
		Route::get('link-barcode', 'CustomerController@linkBarcode')->name('link.barcode');

		Route::get('cancellation', 'CustomerController@cancellation')->name('customer.cancellation');
		Route::get('cancel', 'CustomerController@cancel')->name('customer.cancel');
	});
});

Route::get('flush', function(){
    request()->session()->flush();
});



Route::get("/check-shopify-webhook","CheckShopifyWebhook@get");
Route::get("/delete","CheckShopifyWebhook@destroy");
